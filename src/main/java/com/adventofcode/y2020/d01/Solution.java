package com.adventofcode.y2020.d01;

public class Solution {

    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        int curI = -1, curJ = -1;
        OUTER:
        for (int i = 0; i < Constants.EXPENSE_REPORT.length - 1; i++) {
            curI = Constants.EXPENSE_REPORT[i];
            for (int j = i + 1; j < Constants.EXPENSE_REPORT.length; j++) {
                curJ = Constants.EXPENSE_REPORT[j];
                if (curI + curJ == 2020) {
                    break OUTER;
                }
            }
            curI = -1;
            curJ = -1;
        }
        System.out.println((long) curI * (long) curJ);
    }

    private static void part2() {
        int curI = -1, curJ = -1, curK = -1;
        OUTER:
        for (int i = 0; i < Constants.EXPENSE_REPORT.length - 2; i++) {
            curI = Constants.EXPENSE_REPORT[i];
            for (int j = i + 1; j < Constants.EXPENSE_REPORT.length - 1; j++) {
                curJ = Constants.EXPENSE_REPORT[j];
                for (int k = j + 1; k < Constants.EXPENSE_REPORT.length; k++) {
                    curK = Constants.EXPENSE_REPORT[k];
                    if (curI + curJ + curK == 2020) {
                        break OUTER;
                    }
                }
            }
            curI = -1;
            curJ = -1;
            curK = -1;
        }
        System.out.println((long) curI * (long) curJ * (long) curK);
    }
}
