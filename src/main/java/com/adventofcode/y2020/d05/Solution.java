package com.adventofcode.y2020.d05;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> input = Util.readAllLines(Solution.class, "input");
//        for (String line : input) {
//            System.out.format("%s => %s => %d%n", line, Arrays.toString(getSeat(line)), getSeatId(line));
//        }
        int min = input.stream().mapToInt(Solution::getSeatId).min().orElse(-1);
        int max = input.stream().mapToInt(Solution::getSeatId).max().orElse(-1);
        int totalSum = IntStream.range(min, max + 1).sum();
        int actualSum = input.stream().mapToInt(Solution::getSeatId).sum();
        System.out.println(input.stream().mapToInt(Solution::getSeatId).sorted().boxed().collect(Collectors.toList()));
        System.out.println(totalSum - actualSum);
    }

    private static int getSeatId(String line) {
        int[] seat = getSeat(line);
        return seat[0] * 8 + seat[1];
    }

    private static int[] getSeat(String line) {
        int row = getRow(line, 0, 127);
        int col = getColumn(line, 0, 7);

        return new int[]{row, col};
    }

    private static int getRow(String line, int lo, int hi) {
        if (lo == hi) {
            return lo;
        }
        if (line == null || line.isEmpty()) {
            return -1;
        }
        int mid = (lo + hi + 1) >> 1;
        if (line.charAt(0) == 'F') {
            return getRow(line.substring(1), lo, mid - 1);
        }
        if (line.charAt(0) == 'B') {
            return getRow(line.substring(1), mid, hi);
        }
        return getRow(line.substring(1), lo, hi);
    }

    private static int getColumn(String line, int lo, int hi) {
        if (lo == hi) {
            return lo;
        }
        if (line == null || line.isEmpty()) {
            return -1;
        }
        int mid = (lo + hi + 1) >> 1;
        if (line.charAt(0) == 'L') {
            return getColumn(line.substring(1), lo, mid - 1);
        }
        if (line.charAt(0) == 'R') {
            return getColumn(line.substring(1), mid, hi);
        }
        return getColumn(line.substring(1), lo, hi);
    }

}
