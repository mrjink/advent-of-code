package com.adventofcode.y2020.d02;

import java.util.function.Predicate;

public class Validator1 implements Predicate<Password> {

    @Override
    public boolean test(Password password) {
        if (password.getPassword().length() < password.getMin()) {
            return false;
        }
        int count = 0;
        for (int i = 0; i < password.getPassword().length(); i++) {
            if (password.getPassword().charAt(i) == password.getChr()) {
                count++;
            }
        }
        return count >= password.getMin() && count <= password.getMax();
    }
}
