package com.adventofcode.y2020.d02;

import java.util.function.Predicate;

public class Validator2 implements Predicate<Password> {

    @Override
    public boolean test(Password password) {
        boolean result = false;

        if (password.getMin() <= password.getPassword().length()) {
            result ^= password.getPassword().charAt(password.getMin() - 1) == password.getChr();
        }
        if (password.getMax() <= password.getPassword().length()) {
            result ^= password.getPassword().charAt(password.getMax() - 1) == password.getChr();
        }

        return result;
    }
}
