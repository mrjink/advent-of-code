package com.adventofcode.y2020.d02;

import java.util.regex.Matcher;

public class Password {
    private final int min;
    private final int max;
    private final char chr;
    private final String password;

    private Password(int min, int max, char chr, String password) {
        if (min <= 0 || max <= 0 || max < min || password == null) {
            throw new IllegalArgumentException();
        }
        this.min = min;
        this.max = max;
        this.chr = chr;
        this.password = password;
    }

    static Password fromString(String string) {
        Matcher matcher = Constants.PATTERN.matcher(string);

        if (!matcher.find()) {
            throw new IllegalArgumentException(string);
        }

        return new Password(
                Integer.parseInt(matcher.group(1)),
                Integer.parseInt(matcher.group(2)),
                matcher.group(3).charAt(0),
                matcher.group(4));
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public char getChr() {
        return chr;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return password + " " + chr + "[" + min + "," + max + "]";
    }
}
