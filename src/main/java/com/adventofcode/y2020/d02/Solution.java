package com.adventofcode.y2020.d02;

import java.util.function.Predicate;

public class Solution {
    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        checkPasswords(new Validator1());
    }

    private static void part2() {
        checkPasswords(new Validator2());
    }

    private static void checkPasswords(Predicate<Password> validator) {
        Constants.PASSWORDS.forEach(password -> System.out.println(password + " => " + validator.test(password)));
        System.out.println(Constants.PASSWORDS.stream()
                .filter(validator)
                .count());
    }
}
