package com.adventofcode.y2020.d10;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {
        part1("test_input_1");
        part1("test_input_2");
        part1("input");
        System.out.println();
        part2("test_input_1");
        part2("test_input_2");
    }

    private static void part1(String name) throws URISyntaxException, IOException {
        int[] ints = getArray(name);
        System.out.println(Arrays.toString(ints));
        System.out.println(check1(ints));
    }

    private static void part2(String name) throws URISyntaxException, IOException {
        int[] ints = getArray(name);
        System.out.println(Arrays.toString(ints));
        System.out.println(check2(ints));
    }

    private static int check1(int[] ints) {
        int c1 = 0;
        int c3 = 1;
        int prev = 0;
        for (int n : ints) {
            int diff = n - prev;
            if (diff == 1) {
                c1++;
            } else if (diff == 2) {
                // nothing here
            } else if (diff == 3) {
                c3++;
            } else {
                throw new IllegalStateException();
            }
            prev = n;
        }
        System.out.format("%d %d%n", c1, c3);
        return c1 * c3;
    }

    private static int check2(int[] ints) {
        return 0;
    }

    private static int[] getArray(String name) throws URISyntaxException, IOException {
        List<String> input = Util.readAllLines(Solution.class, name);
        return input.stream().mapToInt(Integer::parseInt).sorted().toArray();
    }
}
