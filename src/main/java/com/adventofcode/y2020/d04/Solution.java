package com.adventofcode.y2020.d04;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class Solution {

    public static void main(String[] args) throws IOException, URISyntaxException {
        String input = Util.readFile(Solution.class, "input");
        String[] passports = input.split("\n\n");
        int count = 0;
        for (String passport : passports) {
            if (checkPassport(passport)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean checkPassport(String passport) {
        Map<String, String> map = new HashMap<>();
        String[] fields = passport.split("\\s+");
        for (String field : fields) {
            String[] values = field.split(":", 2);
            map.put(values[0], values[1]);
        }
        for (Field field : Field.values()) {
            if (!field.isOptional() && !map.containsKey(field.getField())) {
                return false;
            }
            if (map.containsKey(field.getField())) {
                String value = map.remove(field.getField());
                if (!field.isValid(value)) {
                    return false;
                }
            }
        }
        return true;
    }

}
