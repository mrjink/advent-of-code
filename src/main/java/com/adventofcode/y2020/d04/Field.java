package com.adventofcode.y2020.d04;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public enum Field {
    BYR("byr", new YearPredicate(1920, 2002), false), // (Birth Year)
    IYR("iyr", new YearPredicate(2010, 2020), false), // (Issue Year)
    EYR("eyr", new YearPredicate(2020, 2030), false), // (Expiration Year)
    HGT("hgt", new HeightPredicate(), false), // (Height)
    HCL("hcl", new HairColorPredicate(), false), // (Hair Color)
    ECL("ecl", new EyeColorPredicate(), false), // (Eye Color)
    PID("pid", new PassportIdPredicate(), false), // (Passport ID)
    CID("cid", new CountryIdPredicate(), true), // (Country ID)
    ;

    private final String field;
    private final Predicate<String> predicate;
    private final boolean optional;

    Field(String field, Predicate<String> predicate, boolean optional) {
        this.field = field;
        this.predicate = predicate;
        this.optional = optional;
    }

    boolean isValid(String value) {
        return predicate.test(value);
    }

    public String getField() {
        return field;
    }

    public boolean isOptional() {
        return optional;
    }
}

class YearPredicate implements Predicate<String> {
    private final int min, max;

    YearPredicate(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean test(String s) {
        try {
            int i = Integer.parseInt(s);
            return i >= min && i <= max;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

class HeightPredicate implements Predicate<String> {
    @Override
    public boolean test(String s) {
        try {
            if (s.endsWith("cm")) {
                int i = Integer.parseInt(s.substring(0, s.length() - 2));
                return i >= 150 && i <= 193;
            }
            if (s.endsWith("in")) {
                try {
                    int i = Integer.parseInt(s.substring(0, s.length() - 2));
                    return i >= 59 && i <= 76;
                } catch (NumberFormatException e) {
                    return false;
                }
            }
        } catch (NumberFormatException ignored) {
        }
        return false;
    }
}

class HairColorPredicate implements Predicate<String> {
    private static final Pattern PATTERN = Pattern.compile("#[0-9a-f]{6}");

    @Override
    public boolean test(String s) {
        return PATTERN.matcher(s).matches();
    }
}

class EyeColorPredicate implements Predicate<String> {
    @Override
    public boolean test(String s) {
        switch (s) {
            case "amb":
            case "blu":
            case "brn":
            case "gry":
            case "grn":
            case "hzl":
            case "oth":
                return true;
            default:
                return false;
        }
    }
}

class PassportIdPredicate implements Predicate<String> {
    private static final Pattern PATTERN = Pattern.compile("[0-9]{9}");

    @Override
    public boolean test(String s) {
        return PATTERN.matcher(s).matches();
    }
}

class CountryIdPredicate implements Predicate<String> {
    @Override
    public boolean test(String s) {
        return true;
    }
}