package com.adventofcode.y2020.d09;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {
        part1();
        part2();
    }

    private static void part1() throws URISyntaxException, IOException {
        System.out.println(check("test_input", 5));
        System.out.println(check("input", 25));
    }

    private static void part2() throws IOException, URISyntaxException {
        long invalid = check("test_input", 5);
        System.out.println(find("test_input", invalid));
        invalid = check("input", 25);
        System.out.println(find("input", invalid));
    }

    private static long find(String name, long invalid) throws IOException, URISyntaxException {
        List<String> input = Util.readAllLines(Solution.class, name);
        long[] longs = input.stream().mapToLong(Long::parseLong).toArray();
        List<Long> list;
        for (int i = 0; i < longs.length - 1; i++) {
            long sum = longs[i];
            list = new ArrayList<>();
            list.add(sum);
            for (int j = i + 1; j < longs.length; j++) {
                sum += longs[j];
                list.add(longs[j]);
                if (sum == invalid) {
                    return list.stream().reduce(Long.MAX_VALUE, Long::min) + list.stream().reduce(0L, Long::max);
                }
                if (sum > invalid) {
                    break;
                }
            }
        }
        return 0;
    }

    private static long check(String name, int preamble_length) throws URISyntaxException, IOException {
        List<String> input = Util.readAllLines(Solution.class, name);
        long[] longs = input.stream().mapToLong(Long::parseLong).toArray();
        return check(longs, preamble_length);
    }

    private static long check(long[] longs, int preamble_length) {
        //        System.out.println(Arrays.toString(longs));
        long[] preamble = new long[preamble_length];
        LOOP:
        for (int i = 0; i < longs.length - preamble_length; i++) {
            System.arraycopy(longs, i, preamble, 0, preamble_length);
            long next = longs[i + preamble_length];
            //            System.out.format("%s => %d%n", Arrays.toString(preamble), next);
            for (int j = 0; j < preamble_length - 1; j++) {
                for (int k = j + 1; k < preamble_length; k++) {
                    if (preamble[j] + preamble[k] == next) {
                        continue LOOP;
                    }
                }
            }
            return next;
        }
        return -1;
    }
}
