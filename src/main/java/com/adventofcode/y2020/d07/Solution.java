package com.adventofcode.y2020.d07;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Solution {

    private static final String TARGET_COLOR = "shiny gold";
    private static final Pattern LINE_PATTERN = Pattern.compile("^([a-z ]+) bags contain ([,a-z0-9 ]+)\\.$");
    private static final Pattern BAG_PATTERN = Pattern.compile("(\\d+) ([a-z ]+) bags?");
    private static final Map<String, Bag> BAG_CACHE = new TreeMap<>();

    public static void main(String[] args) throws IOException, URISyntaxException {
        fillCache("input");
        printCache();
        part1();
        part2();
    }

    private static void fillCache(String name) throws URISyntaxException, IOException {
        List<String> input = Util.readAllLines(Solution.class, name);
        for (String line : input) {
            Matcher lineMatcher = LINE_PATTERN.matcher(line);
            if (lineMatcher.matches()) {
                Bag bag = getBag(lineMatcher.group(1));
                String contents = lineMatcher.group(2);
                if (!"no other bags".equals(contents)) {
                    Matcher bagMatcher = BAG_PATTERN.matcher(contents);
                    while (bagMatcher.find()) {
                        bag.add(getBag(bagMatcher.group(2)), Integer.parseInt(bagMatcher.group(1)));
                    }
                }
            }
        }
    }

    private static void printCache() {
        BAG_CACHE.values().forEach(System.out::println);
    }

    private static void part1() {
        int count = 0;
        for (String color : BAG_CACHE.keySet()) {
            if (findBag(color)) {
                System.out.format("Found %s in %s%n", TARGET_COLOR, color);
                count++;
            }
        }
        System.out.println(count);
    }

    private static void part2() {
        System.out.println(countBag(TARGET_COLOR) - 1);
    }

    private static int countBag(String color) {
        int count = 1;
        Bag bag = getBag(color);
        for (Bag bagBag : bag.getBags()) {
            count += bag.getCount(bagBag) * countBag(bagBag.getColor());
        }
        return count;
    }

    private static Bag getBag(String color) {
        return BAG_CACHE.computeIfAbsent(color, Bag::new);
    }

    private static boolean findBag(String color) {
        Bag bag = getBag(color);
        if (bag.getBags().contains(getBag(TARGET_COLOR))) {
            return true;
        }
        for (Bag bagBag : bag.getBags()) {
            if (findBag(bagBag.getColor())) {
                return true;
            }
        }
        return false;
    }
}

class Bag implements Comparable<Bag> {
    private final String color;
    private final Map<Bag, Integer> bags;

    Bag(String color) {
        this.color = color;
        this.bags = new TreeMap<>();
    }

    public String getColor() {
        return color;
    }

    public Set<Bag> getBags() {
        return Collections.unmodifiableSet(bags.keySet());
    }

    public int getCount(Bag bag) {
        return bags.getOrDefault(bag, 0);
    }

    public void add(Bag bag, int count) {
        bags.put(bag, count);
    }

    @Override
    public String toString() {
        String bagString = bags.entrySet().stream()
                .map(e -> e.getValue() + " " + e.getKey().color)
                .collect(Collectors.joining(", ", "[", "]"));
        return String.format("%s bag contains %s", color, bagString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bag bag = (Bag) o;
        return color.equals(bag.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }

    @Override
    public int compareTo(Bag o) {
        return color.compareTo(o.color);
    }
}
