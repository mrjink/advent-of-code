package com.adventofcode.y2020.d06;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {

        String group1 = "abcx\n"
                + "abcy\n"
                + "abcz";
        System.out.format("%s => %d%n", group1, count2(group1));

        String input = Util.readFile(Solution.class, "input");
        String[] groups = input.split("\n\n");
//        System.out.println(Arrays.stream(groups).peek(System.out::println).mapToLong(Solution::count1).peek(System.out::println).sum());
        System.out.println(Arrays.stream(groups).peek(System.out::println).mapToLong(Solution::count2).peek(System.out::println).sum());
    }

    private static long count1(String group) {
        return group.chars().filter(c -> c >= 'a' && c <= 'z').distinct().count();
    }

    private static long count2(String group) {
        int groups = group.split("\n").length;
        return group.chars()
                .filter(c -> c >= 'a' && c <= 'z')
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() == groups)
                .count();
    }

}
