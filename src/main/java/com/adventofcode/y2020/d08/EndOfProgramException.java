package com.adventofcode.y2020.d08;

class EndOfProgramException extends ProgramException {
    EndOfProgramException(int pc, int acc) {
        super(pc, acc);
    }
}
