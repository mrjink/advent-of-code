package com.adventofcode.y2020.d08;

class Instruction {
    private final Command command;
    private final int argument;

    private Instruction(Command command, int argument) {
        this.command = command;
        this.argument = argument;
    }

    static Instruction fromString(String string) {
        String[] parts = string.split(" ");
        if (parts.length != 2) {
            throw new IllegalArgumentException(string);
        }
        return new Instruction(Command.fromString(parts[0]), Integer.parseInt(parts[1]));
    }

    public Command getCommand() {
        return command;
    }

    public int getArgument() {
        return argument;
    }

    public Instruction flip() {
        switch (command) {
            case NOP:
                return new Instruction(Command.JMP, argument);
            case JMP:
                return new Instruction(Command.NOP, argument);
            case ACC:
            default:
                return this;
        }
    }
}
