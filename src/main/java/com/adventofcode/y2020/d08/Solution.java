package com.adventofcode.y2020.d08;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.BitSet;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> input = Util.readAllLines(Solution.class, "input");
        part1(input);
        part2(input);
    }

    private static void part1(List<String> input) {
        List<Instruction> instructions = getInstructions(input);
        try {
            run(instructions);
        } catch (ProgramException e) {
            System.out.format("pc=%s, acc=%d%n", e.getPc(), e.getAcc());
        }
    }

    private static void part2(List<String> input) {
        for (int i = 0; i < input.size(); i++) {
            List<Instruction> instructions = getInstructions(input);
            instructions.set(i, instructions.get(i).flip());
            try {
                run(instructions);
            } catch (EndOfProgramException e) {
                System.out.format("i=%d, pc=%d, acc=%d%n", i, e.getPc(), e.getAcc());
            } catch (IllegalPcException | LoopProgramException ignored) {
            }
        }
    }

    private static List<Instruction> getInstructions(List<String> input) {
        return input.stream()
                .map(Instruction::fromString)
                .collect(Collectors.toList());
    }

    private static void run(List<Instruction> instructions)
            throws LoopProgramException, IllegalPcException, EndOfProgramException {
        int pc = 0;
        int acc = 0;
        BitSet bitSet = new BitSet(instructions.size());
        while (true) {
            if (bitSet.get(pc)) {
                throw new LoopProgramException(pc, acc);
            }
            if (pc < 0 || pc > instructions.size()) {
                throw new IllegalPcException(pc, acc);
            }
            if (pc == instructions.size()) {
                throw new EndOfProgramException(pc, acc);
            }
            bitSet.set(pc);
            Instruction instruction = instructions.get(pc);
            switch (instruction.getCommand()) {
                case NOP:
                    pc++;
                    break;
                case ACC:
                    acc += instruction.getArgument();
                    pc++;
                    break;
                case JMP:
                    pc += instruction.getArgument();
                    break;
            }
        }
    }
}
