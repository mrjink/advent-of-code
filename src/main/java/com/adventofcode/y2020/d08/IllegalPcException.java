package com.adventofcode.y2020.d08;

class IllegalPcException extends ProgramException {
    IllegalPcException(int pc, int acc) {
        super(pc, acc);
    }
}
