package com.adventofcode.y2020.d08;

import java.util.Arrays;

enum Command {
    NOP("nop"),
    ACC("acc"),
    JMP("jmp");

    private final String command;

    Command(String command) {
        this.command = command;
    }

    static Command fromString(String command) {
        return Arrays.stream(values())
                .filter(c -> c.command.equals(command))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(command));
    }
}
