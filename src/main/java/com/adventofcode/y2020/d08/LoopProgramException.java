package com.adventofcode.y2020.d08;

class LoopProgramException extends ProgramException {
    LoopProgramException(int pc, int acc) {
        super(pc, acc);
    }
}
