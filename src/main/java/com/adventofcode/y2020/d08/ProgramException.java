package com.adventofcode.y2020.d08;

abstract class ProgramException extends Exception {
    private final int pc;
    private final int acc;

    ProgramException(int pc, int acc) {
        this.pc = pc;
        this.acc = acc;
    }

    public int getPc() {
        return pc;
    }

    public int getAcc() {
        return acc;
    }
}
