package com.adventofcode.y2020.d11;

import java.util.Arrays;

public enum Tile {
    EMPTY('L'),
    OCCUPIED('#'),
    FLOOR('.');

    private final char c;

    Tile(char c) {
        this.c = c;
    }

    public static Tile fromChar(char c) {
        return Arrays.stream(values())
                .filter(e -> e.c == c)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public char getChar() {
        return c;
    }

    @Override
    public String toString() {
        return "" + c;
    }
}
