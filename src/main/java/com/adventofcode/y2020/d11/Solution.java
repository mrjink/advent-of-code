package com.adventofcode.y2020.d11;

import com.adventofcode.util.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class Solution {
    public static void main(String[] args) throws IOException, URISyntaxException {
        Tile[][] prev = new Tile[0][0];
        Tile[][] tiles = readTiles("input");
        int iterations = 0;
        while (!Arrays.deepEquals(prev, tiles)) {
            print(tiles);
            prev = tiles;
            tiles = iterate(prev);
            iterations++;
        }
        System.out.println(iterations - 1);
        System.out.println(occupied(tiles));
    }

    private static int occupied(Tile[][] tiles) {
        return (int) Arrays.stream(tiles)
                .flatMapToLong(
                        row -> LongStream.of(Arrays.stream(row)
                                .filter(t -> t == Tile.OCCUPIED)
                                .count()))
                .sum();
    }

    private static Tile[][] iterate(Tile[][] tiles) {
        Tile[][] result = new Tile[tiles.length][tiles[0].length];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                switch (tiles[i][j]) {
                    case EMPTY:
                        if (count(tiles, i, j, Tile.OCCUPIED) == 0) {
                            result[i][j] = Tile.OCCUPIED;
                        } else {
                            result[i][j] = Tile.EMPTY;
                        }
                        break;
                    case OCCUPIED:
                        if (count(tiles, i, j, Tile.OCCUPIED) >= 4) {
                            result[i][j] = Tile.EMPTY;
                        } else {
                            result[i][j] = Tile.OCCUPIED;
                        }
                        break;
                    case FLOOR:
                        result[i][j] = Tile.FLOOR;
                        break;
                }
            }
        }
        return result;
    }

    private static int[][] counts(Tile[][] tiles, Tile tile) {
        int[][] counts = new int[tiles.length][tiles[0].length];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                counts[i][j] = count(tiles, i, j, tile);
            }
        }
        return counts;
    }

    private static int count(Tile[][] tiles, int y, int x, Tile tile) {
        if (tiles[y][x] == Tile.FLOOR) {
            return -1;
        }
        int count = 0;
        for (int i = Math.max(0, y - 1); i <= Math.min(y + 1, tiles.length - 1); i++) {
            for (int j = Math.max(0, x - 1); j <= Math.min(x + 1, tiles[y].length - 1); j++) {
                if (i == y && j == x) {
                    continue;
                }
                if (tiles[i][j] == tile) {
                    count++;
                }
            }
        }
        return count;
    }

    private static Tile[][] readTiles(String name) throws IOException, URISyntaxException {
        List<String> strings = Util.readAllLines(Solution.class, name);
        Tile[][] result = new Tile[strings.size()][strings.get(0).length()];
        for (int i = 0; i < result.length; i++) {
            String row = strings.get(i);
            for (int j = 0; j < row.length(); j++) {
                result[i][j] = Tile.fromChar(row.charAt(j));
            }
        }
        return result;
    }

    private static void print(Tile[][] tiles) {
        Arrays.stream(tiles)
                .forEach(Solution::print);
        System.out.println();
    }

    private static void print(Tile[] row) {
        System.out.println(Arrays.stream(row)
                .map(Enum::toString)
                .collect(Collectors.joining("")));
    }

    private static void print(int[][] counts) {
        Arrays.stream(counts)
                .forEach(Solution::print);
        System.out.println();
    }

    private static void print(int[] row) {
        StringBuilder result = new StringBuilder();
        for (int c : row) {
            result.append(c);
        }
        System.out.println(result.toString());
    }
}
