package com.adventofcode.y2020.d03;

public class Solution {

    public static void main(String[] args) {
        System.out.println(part1(Constants.TEST_TREES));
        System.out.println(part1(Constants.TREES));
        System.out.println(part2(Constants.TEST_TREES));
        System.out.println(part2(Constants.TREES));
    }

    private static int part1(String[] trees) {
        return countTrees(trees, 3, 1);
    }

    private static long part2(String[] trees) {
        long result = 1L;
        result *= countTrees(trees, 1, 1);
        result *= countTrees(trees, 3, 1);
        result *= countTrees(trees, 5, 1);
        result *= countTrees(trees, 7, 1);
        result *= countTrees(trees, 1, 2);
        return result;
    }

    private static int countTrees(String[] trees, int xStep, int yStep) {
        int result = 0;
        for (int i = 0, j = 0; i < trees.length; i += yStep, j = (j + xStep) % trees[0].length()) {
            if (trees[i].charAt(j) == '#') {
                result++;
            }
        }
        return result;
    }
}
