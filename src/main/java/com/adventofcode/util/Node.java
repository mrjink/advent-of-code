package com.adventofcode.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Node {
    private final String name;
    private final List<Node> shortestPath = new LinkedList<>();
    private final Map<Node, Integer> neighbors = new HashMap<>();

    private int distance = Integer.MAX_VALUE;

    public Node(String name) {
        this.name = name;
    }

    public void addNeighbor(Node node, int distance) {
        neighbors.put(node, distance);
    }

    public String getName() {
        return name;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Map<Node, Integer> getNeighbors() {
        return neighbors;
    }

    public List<Node> getShortestPath() {
        return shortestPath;
    }

    public void setShortestPathVia(Node node) {
        shortestPath.clear();
        shortestPath.addAll(node.getShortestPath());
        shortestPath.add(node);
    }

    @Override
    public String toString() {
        return "Node{name='" + name + '\'' + '}';
    }
}
