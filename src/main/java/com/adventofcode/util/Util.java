package com.adventofcode.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class Util {
    private Util() {
    }

    public static Path getPath(Class<?> clazz, String name) throws URISyntaxException {
        URL url = clazz.getResource(name);
        return Paths.get(url.toURI());
    }

    public static String readFile(Class<?> clazz, String name)
            throws URISyntaxException, IOException {
        Path path = getPath(clazz, name);
        return Files.readString(path, StandardCharsets.UTF_8);
    }

    public static List<String> readAllLines(Class<?> clazz, String name)
            throws URISyntaxException, IOException {
        Path path = getPath(clazz, name);
        return Files.readAllLines(path, StandardCharsets.UTF_8);
    }

    public static int[] readAllInts(Class<?> clazz, String name)
            throws URISyntaxException, IOException, NumberFormatException {
        return readAllLines(clazz, name).stream().mapToInt(Integer::parseInt).toArray();
    }

    public static <E> List<List<E>> generatePerm(List<E> original) {
        if (original.isEmpty()) {
            List<List<E>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<>();
        List<List<E>> permutations = generatePerm(original);
        for (List<E> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<E> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

    public static int[][] toArray(List<String> input) {
        return input.stream()
                .map(s -> Arrays.stream(s.split(""))
                        .mapToInt(Integer::parseInt)
                        .toArray())
                .toArray(int[][]::new);
    }

    public static int getCellOr(int[][] ints, int x, int y, int value) {
        try {
            return ints[y][x];
        } catch (ArrayIndexOutOfBoundsException e) {
            return value;
        }
    }

    public static int getTop(int[][] ints, int x, int y, int value) {
        return getCellOr(ints, x, y - 1, value);
    }

    public static int getLeft(int[][] ints, int x, int y, int value) {
        return getCellOr(ints, x - 1, y, value);
    }

    public static int getBottom(int[][] ints, int x, int y, int value) {
        return getCellOr(ints, x, y + 1, value);
    }

    public static int getRight(int[][] ints, int x, int y, int value) {
        return getCellOr(ints, x + 1, y, value);
    }

    public static <T> Optional<T> getCellOr(T[][] grid, int x, int y, T value) {
        try {
            return Optional.ofNullable(grid[y][x]);
        } catch (ArrayIndexOutOfBoundsException e) {
            return Optional.ofNullable(value);
        }
    }

    public static <T> Optional<T> getTop(T[][] grid, int x, int y, T value) {
        return getCellOr(grid, x, y - 1, value);
    }

    public static <T> Optional<T> getLeft(T[][] grid, int x, int y, T value) {
        return getCellOr(grid, x - 1, y, value);
    }

    public static <T> Optional<T> getBottom(T[][] grid, int x, int y, T value) {
        return getCellOr(grid, x, y + 1, value);
    }

    public static <T> Optional<T> getRight(T[][] grid, int x, int y, T value) {
        return getCellOr(grid, x + 1, y, value);
    }

    public static <T> Optional<T> getTop(T[][] grid, int x, int y) {
        return getCellOr(grid, x, y - 1, null);
    }

    public static <T> Optional<T> getLeft(T[][] grid, int x, int y) {
        return getCellOr(grid, x - 1, y, null);
    }

    public static <T> Optional<T> getBottom(T[][] grid, int x, int y) {
        return getCellOr(grid, x, y + 1, null);
    }

    public static <T> Optional<T> getRight(T[][] grid, int x, int y) {
        return getCellOr(grid, x + 1, y, null);
    }
}
