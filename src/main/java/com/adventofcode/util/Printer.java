package com.adventofcode.util;

import java.util.Locale;

public interface Printer {
    void print(boolean b);

    void print(char c);

    void print(int i);

    void print(long l);

    void print(float f);

    void print(double d);

    void print(char[] s);

    void print(String s);

    void print(Object obj);

    void println();

    void println(boolean x);

    void println(char x);

    void println(int x);

    void println(long x);

    void println(float x);

    void println(double x);

    void println(char[] x);

    void println(String x);

    void println(Object x);

    void printf(String format, Object... args);

    void printf(Locale l, String format, Object... args);

    void format(String format, Object... args);

    void format(Locale l, String format, Object... args);
}
