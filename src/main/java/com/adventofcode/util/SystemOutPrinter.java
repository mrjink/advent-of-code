package com.adventofcode.util;

import java.util.Locale;

public class SystemOutPrinter implements Printer {
    @Override
    public void print(boolean b) {
        System.out.print(b);
    }

    @Override
    public void print(char c) {
        System.out.print(c);
    }

    @Override
    public void print(int i) {
        System.out.print(i);
    }

    @Override
    public void print(long l) {
        System.out.print(l);
    }

    @Override
    public void print(float f) {
        System.out.print(f);
    }

    @Override
    public void print(double d) {
        System.out.print(d);
    }

    @Override
    public void print(char[] s) {
        System.out.print(s);
    }

    @Override
    public void print(String s) {
        System.out.print(s);
    }

    @Override
    public void print(Object obj) {
        System.out.print(obj);
    }

    @Override
    public void println() {
        System.out.println();
    }

    @Override
    public void println(boolean x) {
        System.out.println(x);
    }

    @Override
    public void println(char x) {
        System.out.println(x);
    }

    public void println(int x) {
        System.out.println(x);
    }

    @Override
    public void println(long x) {
        System.out.println(x);
    }

    @Override
    public void println(float x) {
        System.out.println(x);
    }

    @Override
    public void println(double x) {
        System.out.println(x);
    }

    @Override
    public void println(char[] x) {
        System.out.println(x);
    }

    @Override
    public void println(String x) {
        System.out.println(x);
    }

    @Override
    public void println(Object x) {
        System.out.println(x);
    }

    @Override
    public void printf(String format, Object... args) {
        System.out.printf(format, args);
    }

    @Override
    public void printf(Locale l, String format, Object... args) {
        System.out.printf(l, format, args);
    }

    @Override
    public void format(String format, Object... args) {
        System.out.format(format, args);
    }

    @Override
    public void format(Locale l, String format, Object... args) {
        System.out.format(l, format, args);
    }
}
