package com.adventofcode.y2016.d14;

import com.adventofcode.util.Util;
import jakarta.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final char[] HEX =
            new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            String salt = input.get(0);
            part1(salt);
            part2(salt);
        } catch (URISyntaxException | IOException | NoSuchAlgorithmException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String salt) throws NoSuchAlgorithmException {
        solve(salt, 1);
    }

    private static void part2(String salt) throws NoSuchAlgorithmException {
        solve(salt, 2017);
    }

    private static void solve(String salt, int rounds) throws NoSuchAlgorithmException {
        List<String> hashes = new ArrayList<>();
        int found = 0;
        int index = 0;
        do {
            String hash = getHash(hashes, salt, index, rounds);
            String pentuple = findTriple(hash);
            if (pentuple != null) {
                for (int i = 1; i <= 1000; i++) {
                    String next = getHash(hashes, salt, index + i, rounds);
                    if (next.contains(pentuple)) {
                        System.out.printf("Found a key for %d (%s) at %d (%s)%n", index, hash, index + i, next);
                        found++;
                        break;
                    }
                }
            }
            index++;
        } while (found != 64);
    }

    private static String findTriple(String hash) {
        Map<String, Integer> indices = new HashMap<>();
        for (char hex : HEX) {
            String triple = "" + hex + hex + hex;
            int index = hash.indexOf(triple);
            if (index >= 0) {
                indices.put(triple + hex + hex, index);
            }
        }
        Integer min = indices.values().stream().min(Integer::compareTo).orElse(-1);
        return indices.entrySet().stream().filter(e -> e.getValue().equals(min)).map(Map.Entry::getKey).findFirst().orElse(null);
    }

    private static String getHash(List<String> hashes, String salt, int index, int rounds)
            throws NoSuchAlgorithmException {
        if (index < hashes.size()) {
            return hashes.get(index);
        }
        if (index == hashes.size()) {
            String hash = md5(salt + index, rounds);
            hashes.add(hash);
            return hash;
        }
        throw new RuntimeException();
    }

    private static String md5(String data, int rounds) throws NoSuchAlgorithmException {
        String result = data;
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        for (int i = 0; i < rounds; i++) {
            md5.update(result.getBytes());
            result = DatatypeConverter.printHexBinary(md5.digest()).toLowerCase();
        }
        return result;
    }
}
