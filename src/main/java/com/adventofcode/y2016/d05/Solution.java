package com.adventofcode.y2016.d05;

import com.adventofcode.util.Util;
import jakarta.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) throws NoSuchAlgorithmException {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        String doorId = input.get(0);
        long index = 0L;
        int count = 0;

        do {
            md5.update((doorId + index).getBytes());
            byte[] digest = md5.digest();
            String hex = DatatypeConverter.printHexBinary(digest).toLowerCase();
            if (hex.startsWith("00000")) {
                System.out.print(hex.charAt(5));
                count++;
            }
            index++;
        } while (count < 8);
        System.out.println();
    }

    private static void part2(List<String> input) throws NoSuchAlgorithmException {
        char[] password = new char[]{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        String doorId = input.get(0);
        long index = 0L;
        int count = 0;

        do {
            md5.update((doorId + index).getBytes());
            byte[] digest = md5.digest();
            String hex = DatatypeConverter.printHexBinary(digest).toLowerCase();
            if (hex.startsWith("00000")) {
                char pos = hex.charAt(5);
                char c = hex.charAt(6);
                if (pos >= '0' && pos <= '7') {
                    int idx = pos - '0';
                    if (password[idx] == ' ') {
                        password[idx] = c;
                        count++;
                        System.out.println(password);
                    }
                }
            }
            index++;
        } while (count < 8);
        System.out.println(password);
    }
}
