package com.adventofcode.y2016.d09;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final Pattern MARKER = Pattern.compile("\\((\\d+)x(\\d+)\\)");

    public static void main(String[] args) {
        try {
            Path path = Util.getPath(Solution.class, "input");
            part1(path);
            part2(path);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(Path path) throws IOException {
        Path decompressed = decompress(path);
        System.out.format("%s => %d%n", decompressed, decompressed.toFile().length());
    }

    private static void part2(Path path) throws IOException {
        Path decompressed = path;
        while (true) {
            if ((decompressed = decompress(decompressed)) == null) {
                break;
            }
            System.out.format("%s => %d%n", decompressed, decompressed.toFile().length());
        }
    }

    private static Path decompress(Path path) throws IOException {
        Path tempFile = Files.createTempFile("decompress", null);
        boolean containsMarkers = false;

        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()));
             BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile.toFile()))) {
            int i;
            while ((i = reader.read()) != -1) {
                char c = (char) i;
                if (c == '(') {
                    containsMarkers = true;
                    int[] marker = readMarker(reader);
                    char[] buffer = new char[marker[0]];
                    int read = reader.read(buffer, 0, buffer.length);
                    if (read != buffer.length) {
                        System.out.println("Oops");
                    }
                    for (int times = 1; times <= marker[1]; times++) {
                        writer.write(buffer, 0, buffer.length);
                    }
                } else {
                    writer.write(c);
                }
            }
        }
        if (containsMarkers) {
            return tempFile;
        }
        return null;
    }

    private static int[] readMarker(Reader reader) throws IOException {
        StringBuilder marker = new StringBuilder();
        int i;
        while ((i = reader.read()) != -1 && ((char) i != ')')) {
            marker.append((char) i);
        }
        String[] xes = marker.toString().split("x");
        int length = Integer.parseInt(xes[0]);
        int times = Integer.parseInt(xes[1]);
        return new int[]{length, times};
    }

    private static String decompress(String line) {
        String string = "";
        StringBuilder stringBuilder = new StringBuilder();
        Matcher matcher = MARKER.matcher(line);
        int start = 0;
        while (matcher.find(start)) {
            int length = Integer.parseInt(matcher.group(1));
            int times = Integer.parseInt(matcher.group(2));
            try {
                stringBuilder.append(line, start, matcher.start());
            } catch (OutOfMemoryError ignored) {
                string += stringBuilder.toString();
                stringBuilder = new StringBuilder(line.substring(start, matcher.start()));
            }
            try {
                stringBuilder.append(line.substring(matcher.end(), matcher.end() + length).repeat(times));
            } catch (OutOfMemoryError ignored) {
                string += stringBuilder.toString();
                stringBuilder = new StringBuilder(line.substring(matcher.end(), matcher.end() + length).repeat(times));
            }
            start = matcher.end() + length;
        }
        string = string.concat(stringBuilder.toString());
        string = string.concat(line.substring(start));
        System.out.println(string);
        return string;
    }

}
