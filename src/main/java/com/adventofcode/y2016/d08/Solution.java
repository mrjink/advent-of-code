package com.adventofcode.y2016.d08;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern RECT = Pattern.compile("rect (\\d+)x(\\d+)");
    private static final Pattern COL = Pattern.compile("rotate column x=(\\d+) by (\\d+)");
    private static final Pattern ROW = Pattern.compile("rotate row y=(\\d+) by (\\d+)");

    public static void main(String[] args) {
        try {
            // List<String> input = Util.readAllLines(Solution.class, "test_input");
            // boolean[][] screen = new boolean[3][7];

            List<String> input = Util.readAllLines(Solution.class, "input");
            boolean[][] screen = new boolean[6][50];
            part1(screen, input);
            print(screen);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(boolean[][] screen, List<String> input) {
        for (String line : input) {
            Matcher rect = RECT.matcher(line);
            if (rect.find()) {
                int width = Integer.parseInt(rect.group(1));
                int height = Integer.parseInt(rect.group(2));
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        screen[i][j] = true;
                    }
                }
            }
            Matcher col = COL.matcher(line);
            if (col.find()) {
                int x = Integer.parseInt(col.group(1));
                int by = Integer.parseInt(col.group(2));
                boolean[] copy = copyCol(screen, x);
                for (int y = 0; y < screen.length; y++) {
                    screen[y][x] = copy[(screen.length + y - by) % screen.length];
                }
            }
            Matcher row = ROW.matcher(line);
            if (row.find()) {
                int y = Integer.parseInt(row.group(1));
                int by = Integer.parseInt(row.group(2));
                boolean[] copy = copyRow(screen, y);
                for (int x = 0; x < screen[y].length; x++) {
                    screen[y][x] = copy[(screen[y].length + x - by) % screen[y].length];
                }
            }
            // print(screen);
        }
        System.out.println(count(screen));
    }

    private static boolean[] copyCol(boolean[][] screen, int x) {
        boolean[] copy = new boolean[screen.length];
        for (int i = 0; i < screen.length; i++) {
            copy[i] = screen[i][x];
        }
        return copy;
    }

    private static boolean[] copyRow(boolean[][] screen, int y) {
        boolean[] copy = new boolean[screen[y].length];
        for (int i = 0; i < screen[y].length; i++) {
            copy[i] = screen[y][i];
        }
        return copy;
    }

    private static void print(boolean[][] screen) {
        for (boolean[] line : screen) {
            for (boolean b : line) {
                System.out.print(b ? "#" : ".");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static int count(boolean[][] screen) {
        int count = 0;
        for (boolean[] line : screen) {
            for (boolean b : line) {
                if (b) {
                    count++;
                }
            }
        }
        return count;
    }

}
