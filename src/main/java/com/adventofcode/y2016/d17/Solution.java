package com.adventofcode.y2016.d17;

import com.adventofcode.util.Util;
import jakarta.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            List<String> routes = findRoutes(line);
            // System.out.println(routes);
            System.out.println(routes.stream().min(Comparator.comparingInt(String::length)).orElseThrow());
        }
    }

    private static void part2(List<String> input) {
        for (String line : input) {
            List<String> routes = findRoutes(line);
            // System.out.println(routes);
            System.out.println(routes.stream().max(Comparator.comparingInt(String::length)).orElseThrow());
        }
    }

    private static List<String> findRoutes(String input) {
        return findRoutes(input, "", 0, 0);
    }

    private static List<String> findRoutes(String input, String steps, int x, int y) {
        List<String> routes = new ArrayList<>();

        if (x == 3 && y == 3) {
            routes.add(steps);
            return routes;
        }

        EnumSet<Direction> directions = getDirections(input + steps, x, y);
        if (directions.isEmpty()) {
            //            return Collections.emptyList();
            throw new RuntimeException("No possible moves at %dx%d after %s".formatted(x, y, steps));
        }

        for (Direction direction : directions) {
            try {
                switch (direction) {
                    case U -> routes.addAll(findRoutes(input, steps + direction.name(), x, y - 1));
                    case D -> routes.addAll(findRoutes(input, steps + direction.name(), x, y + 1));
                    case L -> routes.addAll(findRoutes(input, steps + direction.name(), x - 1, y));
                    case R -> routes.addAll(findRoutes(input, steps + direction.name(), x + 1, y));
                }
            } catch (RuntimeException e) {
                // System.out.println(e.getMessage());
            }
        }

        return routes;
    }

    private static EnumSet<Direction> getDirections(String input, int x, int y) {
        String digest = md5(input);
        EnumSet<Direction> set = EnumSet.noneOf(Direction.class);
        for (int i = 0; i < 4; i++) {
            char c = digest.charAt(i);
            switch (c) {
                case 'b', 'c', 'd', 'e', 'f' -> set.add(Direction.values()[i]);
            }
        }
        if (y == 0) {
            set.remove(Direction.U);
        }
        if (y == 3) {
            set.remove(Direction.D);
        }
        if (x == 0) {
            set.remove(Direction.L);
        }
        if (x == 3) {
            set.remove(Direction.R);
        }
        // System.out.println(digest.substring(0, 4) + " " + input + " " + set);
        return set;
    }

    private static String md5(String input) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(input.getBytes());
            return DatatypeConverter.printHexBinary(md5.digest()).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Your setup is broken. MD5 should be supported.", e);
        }
    }

}
