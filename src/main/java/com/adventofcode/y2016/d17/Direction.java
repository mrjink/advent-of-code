package com.adventofcode.y2016.d17;

enum Direction {
    U, D, L, R
}
