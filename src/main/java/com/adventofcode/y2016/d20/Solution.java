package com.adventofcode.y2016.d20;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.BitSet;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            BitSet lo = new BitSet();
            BitSet hi = new BitSet();
            hi.set(0, Integer.MAX_VALUE);
            lo.set(0, Integer.MAX_VALUE);
            for (String line : input) {
                String[] range = line.split("-");
                long start = Long.parseLong(range[0]);
                long end = Long.parseLong(range[1]) + 1;
                if (end <= Integer.MAX_VALUE) {
                    lo.clear((int) start, (int) end);
                    continue;
                }
                if (start > Integer.MAX_VALUE) {
                    hi.clear((int) (start - Integer.MAX_VALUE - 1), (int) (Math.min(end - Integer.MAX_VALUE - 1, Integer.MAX_VALUE)));
                    continue;
                }
                lo.clear((int) start, Integer.MAX_VALUE);
                hi.clear(0, (int) (end - Integer.MAX_VALUE - 1));
            }
            part1(lo, hi);
            part2(lo, hi);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(BitSet lo, BitSet hi) {
        long bit = lo.nextSetBit(0);
        if (bit == -1) {
            bit = (long) Integer.MAX_VALUE + 1L + hi.nextSetBit(0);
        }
        System.out.println(bit);
    }

    private static void part2(BitSet lo, BitSet hi) {
        System.out.println((long) lo.cardinality() + hi.cardinality());
    }
}
