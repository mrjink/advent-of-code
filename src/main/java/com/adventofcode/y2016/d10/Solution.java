package com.adventofcode.y2016.d10;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern VALUE = Pattern.compile("^value (\\d+) goes to (bot|output) (\\d+)$");
    private static final Pattern BOT = Pattern.compile("^bot (\\d+) gives low to (bot|output) (\\d+) and high to (bot|output) (\\d+)$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        Map<Integer, Output> outputs = new TreeMap<>();
        Map<Integer, Bot> bots = new TreeMap<>();

        for (String line : input) {
            Matcher m = BOT.matcher(line);
            if (m.matches()) {
                Bot bot = getBot(bots, Integer.parseInt(m.group(1)));
                ValueConsumer low = parse(bots, outputs, m.group(2), Integer.parseInt(m.group(3)));
                ValueConsumer high = parse(bots, outputs, m.group(4), Integer.parseInt(m.group(5)));
                bot.setLow(low);
                bot.setHigh(high);
            }
        }

        System.out.println(bots);
        System.out.println(outputs);

        for (String line : input) {
            Matcher m = VALUE.matcher(line);
            if (m.matches()) {
                Value value = new Value(Integer.parseInt(m.group(1)));
                ValueConsumer vc = parse(bots, outputs, m.group(2), Integer.parseInt(m.group(3)));
                vc.accept(value);
            }
        }
    }

    private static ValueConsumer parse(Map<Integer, Bot> bots, Map<Integer, Output> outputs, String type, int number) {
        return switch (type) {
            case "bot" -> getBot(bots, number);
            case "output" -> getOutput(outputs, number);
            default -> null;
        };
    }

    private static Bot getBot(Map<Integer, Bot> bots, int number) {
        return bots.computeIfAbsent(number, Bot::new);
    }

    private static Output getOutput(Map<Integer, Output> outputs, int number) {
        return outputs.computeIfAbsent(number, Output::new);
    }

}
