package com.adventofcode.y2016.d10;

final class Value implements Comparable<Value> {
    private final int value;

    public Value(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(Value other) {
        return Integer.compare(value, other.value);
    }

    public int getValue() {
        return value;
    }
}
