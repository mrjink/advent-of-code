package com.adventofcode.y2016.d10;

import java.util.ArrayList;
import java.util.List;

class Bot implements ValueConsumer {

    private final int number;
    private List<Value> values;

    private ValueConsumer low;
    private ValueConsumer high;

    public Bot(int number) {
        this.number = number;
        values = new ArrayList<>();
    }

    public void setLow(ValueConsumer low) {
        this.low = low;
    }

    public void setHigh(ValueConsumer high) {
        this.high = high;
    }

    @Override
    public void accept(Value value) {
        System.out.printf("BOT %d accepts value %d%n", number, value.getValue());
        values.add(value);
        if (values.size() == 2) {
            Value min = values.stream().min(Value::compareTo).orElseThrow(IllegalStateException::new);
            Value max = values.stream().max(Value::compareTo).orElseThrow(IllegalStateException::new);
            System.out.printf("BOT %d propagates values (low=%d, high=%d)%n", number, min.getValue(), max.getValue());
            low.accept(min);
            high.accept(max);
            values.clear();
        }
    }
}
