package com.adventofcode.y2016.d10;

import java.util.function.Consumer;

interface ValueConsumer extends Consumer<Value> {
}
