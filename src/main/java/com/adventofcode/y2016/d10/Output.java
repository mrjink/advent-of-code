package com.adventofcode.y2016.d10;

final class Output implements ValueConsumer {
    private final int number;
    private Value value;

    public Output(int number) {
        this.number = number;
        this.value = null;
    }

    @Override
    public void accept(Value value) {
        System.out.printf("OUTPUT %d accepts value %d%n", number, value.getValue());
        this.value = value;
    }

    public int number() {
        return number;
    }
}
