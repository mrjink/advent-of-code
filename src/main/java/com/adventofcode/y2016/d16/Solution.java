package com.adventofcode.y2016.d16;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final int LENGTH = 20;

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "test_input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            fillDisk(line, LENGTH);
        }
        fillDisk("10000", 20);

        fillDisk("11100010111110100", 272);
    }

    private static void part2(List<String> input) {
        fillDisk("11100010111110100", 35651584);
    }

    private static void fillDisk(String input, int length) {
        String result = makeLonger(input, length);
        // System.out.println(result);
        String checksum = calculateChecksum(result.substring(0, length));
        System.out.println(checksum);
    }

    private static String calculateChecksum(String input) {
        String result = input;
        while (result.length() % 2 == 0) {
            StringBuilder checksum = new StringBuilder();
            for (int i = 0; i < result.length(); i += 2) {
                checksum.append(result.charAt(i) == result.charAt(i + 1) ? "1" : "0");
            }
            result = checksum.toString();
        }
        return result;
    }

    private static String makeLonger(String input, int length) {
        String result = input;
        while (result.length() < length) {
            result = step(result);
        }
        return result;
    }

    private static String step(String a) {
        String b = new StringBuilder(a).reverse().toString()
                .replace('1', 'T')
                .replace('0', '1')
                .replace('T', '0');
        return a + "0" + b;
    }
}
