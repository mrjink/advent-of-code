package com.adventofcode.y2016.d04;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final Pattern PATTERN = Pattern.compile("^([a-z-]+)-([0-9]+)\\[([a-z]+)]$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int sum = 0;
        for (String line : input) {
            Matcher m = PATTERN.matcher(line);
            if (m.find()) {
                String name = m.group(1).replaceAll("-", "");
                int sectorID = Integer.parseInt(m.group(2));
                String checksum = m.group(3);
                String calculated = calculateChecksum(name);
                // System.out.printf("%s %d %s %s%n", name, sectorID, checksum, calculated);
                if (checksum.equals(calculated)) {
                    sum += sectorID;
                }
            } else {
                System.out.println(line + " does not match!");
            }
        }
        System.out.println(sum);
    }

    private static String calculateChecksum(String name) {
        Map<Character, Long> map = name.chars()
                .mapToObj(x -> (char) x)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Comparator<Map.Entry<Character, Long>> comp = (e1, e2) -> {
            int compare = Long.compare(e2.getValue(), e1.getValue());
            if (compare != 0) {
                return compare;
            }
            return Character.compare(e1.getKey(), e2.getKey());
        };
        return map.entrySet().stream()
                .sorted(comp)
                .limit(5)
                .map(Map.Entry::getKey)
                .map(c -> "" + c)
                .collect(Collectors.joining(""));
    }

    private static void part2(List<String> input) {
        for (String line : input) {
            Matcher m = PATTERN.matcher(line);
            if (m.find()) {
                String name = m.group(1);
                int sectorID = Integer.parseInt(m.group(2));
                String realName = rotate(name, sectorID);
                if (realName.equals("northpole object storage")) {
                    System.out.printf("%s %s %d%n", name, realName, sectorID);
                }
            } else {
                System.out.println(line + " does not match!");
            }
        }
    }

    private static String rotate(String name, int sectorID) {
        StringBuilder result = new StringBuilder();
        for (char c : name.toCharArray()) {
            if (c == '-') {
                result.append(' ');
            }
            if (c >= 'a' && c <= 'z') {
                result.append((char) ('a' + (((c - 'a') + sectorID) % 26)));
            }
        }
        return result.toString();
    }
}
