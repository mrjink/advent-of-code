package com.adventofcode.y2016.d07;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern BRACKETS = Pattern.compile("\\[([a-z]+)]");
    private static final Pattern ABBA = Pattern.compile("([a-z])(?!\\1)([a-z])\\2\\1");
    private static final Pattern ABA = Pattern.compile("([a-z])(?!\\1)([a-z])\\1");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int count = 0;
        for (String line : input) {
            if (supportsTLS(line)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean supportsTLS(String string) {
        Matcher brackets = BRACKETS.matcher(string);
        while (brackets.find()) {
            Matcher abba = ABBA.matcher(brackets.group(1));
            if (abba.find()) {
                return false;
            }
        }
        for (String part : BRACKETS.split(string)) {
            Matcher abba = ABBA.matcher(part);
            if (abba.find()) {
                return true;
            }
        }
        return false;
    }

    private static void part2(List<String> input) {
        int count = 0;
        for (String line : input) {
            if (supportSSL(line)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean supportSSL(String string) {
        System.out.println(string);
        Matcher brackets = BRACKETS.matcher(string);
        while (brackets.find()) {
            Matcher aba = ABA.matcher(brackets.group(1));
            int start = 0;
            while (aba.find(start)) {
                String match = aba.group(0);
                start = aba.start() + 1;
                String invert = match.substring(1) + match.charAt(1);
                System.out.println(match + " " + invert);
                for (String part : BRACKETS.split(string)) {
                    if (part.contains(invert)) {
                        System.out.println("Found!");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
