package com.adventofcode.y2016.d01;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            Set<String> places = new HashSet<>();
            Direction direction = Direction.NORTH;
            AtomicInteger x = new AtomicInteger();
            AtomicInteger y = new AtomicInteger();
            move:
            for (String move : line.split(", ")) {
                String dir = move.substring(0, 1);
                int by = Integer.parseInt(move.substring(1));
                direction = direction.change(dir);
                direction.move(x, y, by);
            }
            System.out.printf("%s %d %d%n", direction, x.get(), y.get());
            System.out.println(Math.abs(x.get()) + Math.abs(y.get()));
        }
    }

    private static void part2(List<String> input) {
        for (String line : input) {
            Set<String> places = new HashSet<>();
            Direction direction = Direction.NORTH;
            AtomicInteger x = new AtomicInteger();
            AtomicInteger y = new AtomicInteger();
            move:
            for (String move : line.split(", ")) {
                String dir = move.substring(0, 1);
                int by = Integer.parseInt(move.substring(1));
                direction = direction.change(dir);
                for (int i = 1; i <= by; i++) {
                    direction.move(x, y, 1);
                    if (!places.add("%dx%d".formatted(x.get(), y.get()))) {
                        break move;
                    }
                }
            }
            System.out.printf("%s %d %d%n", direction, x.get(), y.get());
            System.out.println(Math.abs(x.get()) + Math.abs(y.get()));
        }
    }
}
