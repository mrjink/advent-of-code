package com.adventofcode.y2016.d01;

import java.util.concurrent.atomic.AtomicInteger;

public enum Direction {
    NORTH {
        @Override
        public void move(AtomicInteger x, AtomicInteger y, int by) {
            y.addAndGet(by);
        }
    },
    EAST {
        @Override
        public void move(AtomicInteger x, AtomicInteger y, int by) {
            x.addAndGet(by);
        }
    },
    SOUTH {
        @Override
        public void move(AtomicInteger x, AtomicInteger y, int by) {
            y.addAndGet(-by);
        }
    },
    WEST {
        @Override
        public void move(AtomicInteger x, AtomicInteger y, int by) {
            x.addAndGet(-by);
        }
    };

    public Direction change(String to) {
        return switch (to) {
            case "L" -> values()[(ordinal() + 3) % 4];
            case "R" -> values()[(ordinal() + 1) % 4];
            default -> this;
        };
    }

    public abstract void move(AtomicInteger x, AtomicInteger y, int by);
}
