package com.adventofcode.y2016.d13;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            int number = Integer.parseInt(input.get(0));
            part1(number);
            part2(number);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int number) {
        InfiniteGrid infiniteGrid = new InfiniteGrid(number);
        infiniteGrid.setNumber(1, 1, 0);
        infiniteGrid.fillNeighbors(1, 1);
        printGridValues(infiniteGrid);
        System.out.println(infiniteGrid.getNumber(31, 39));
    }

    private static void part2(int number) {
        int count = 0;
        InfiniteGrid infiniteGrid = new InfiniteGrid(number);
        infiniteGrid.setNumber(1, 1, 0);
        infiniteGrid.fillNeighbors(1, 1);
        for (int y = 0; y < infiniteGrid.getHeight(); y++) {
            for (int x = 0; x < infiniteGrid.getWidth(); x++) {
                int value = infiniteGrid.getNumber(x, y);
                if (value <= 50) {
                    count++;
                }
            }
        }
        System.out.println(count);
    }

    private static void printGridValues(InfiniteGrid infiniteGrid) {
        int width = infiniteGrid.getWidth();
        int height = infiniteGrid.getHeight();

        // header
        System.out.print("  ");
        for (int x = 0; x < width; x++) {
            System.out.printf("%4d", Math.abs(x % 10));
        }
        System.out.println();

        // grid
        for (int y = 0; y < height; y++) {
            System.out.print(Math.abs(y % 10));
            System.out.print(" ");
            for (int x = 0; x < width; x++) {
                int value = infiniteGrid.getNumber(x, y);
                boolean isOpen = infiniteGrid.isOpen(x, y);
                System.out.printf("%4s", isOpen ? ((value == Integer.MAX_VALUE) ? "?" : value) : "###");
            }
            System.out.println();
        }
    }

    private static void printGrid(InfiniteGrid infiniteGrid) {
        int width = infiniteGrid.getWidth();
        int height = infiniteGrid.getHeight();

        // header
        System.out.print("  ");
        for (int x = 0; x < width; x++) {
            System.out.print(Math.abs(x % 10));
        }
        System.out.println();

        // grid
        for (int y = 0; y < height; y++) {
            System.out.print(Math.abs(y % 10));
            System.out.print(" ");
            for (int x = 0; x < width; x++) {
                boolean isOpen = infiniteGrid.isOpen(x, y);
                System.out.print(isOpen ? "." : "#");
            }
            System.out.println();
        }
    }

}
