package com.adventofcode.y2016.d13;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class InfiniteGrid {
    private final int seed;
    private final List<BitSet> bitLines;
    private final List<BitSet> cache;
    private final List<List<Integer>> numberLines;

    public InfiniteGrid(int seed) {
        this.seed = seed;
        this.bitLines = new ArrayList<>();
        this.cache = new ArrayList<>();
        this.numberLines = new ArrayList<>();
    }

    public boolean isOpen(int x, int y) {
        if (x < 0 || y < 0) {
            return false;
        }
        if (y < cache.size()) {
            BitSet cachedLine = cache.get(y);
            if (cachedLine.get(x)) {
                return bitLines.get(y).get(x);
            }
            boolean open = isOpen0(x, y);
            cachedLine.set(x);
            bitLines.get(y).set(x, open);
            return open;
        }
        for (int i = cache.size(); i <= y; i++) {
            bitLines.add(new BitSet());
            cache.add(new BitSet());
        }
        return isOpen(x, y);
    }

    public void setNumber(int x, int y, int number) {
        // cheat instead of ensuring capacity
        getNumber(x, y);
        numberLines.get(y).set(x, number);
    }

    public int getNumber(int x, int y) {
        if (x < 0 || y < 0 || !isOpen(x, y)) {
            return Integer.MAX_VALUE;
        }
        if (y < numberLines.size()) {
            List<Integer> line = numberLines.get(y);
            if (x < line.size()) {
                return line.get(x);
            }
            for (int i = line.size(); i <= x; i++) {
                line.add(Integer.MAX_VALUE);
            }
        }
        for (int i = numberLines.size(); i <= y; i++) {
            numberLines.add(new ArrayList<>());
        }
        return getNumber(x, y);
    }

    public int getHeight() {
        return numberLines.size();
    }

    public int getWidth() {
        return numberLines.stream().mapToInt(List::size).max().orElse(0);
    }

    public boolean fillNeighbors(int x, int y) {
        boolean changes = false;
        int value = getNumber(x, y);
        if (isOpen(x, y - 1) && value < getNumber(x, y - 1)) {
            setNumber(x, y - 1, value + 1);
            changes = fillNeighbors(x, y - 1);
        }

        if (isOpen(x - 1, y) && value < getNumber(x - 1, y)) {
            setNumber(x - 1, y, value + 1);
            changes = fillNeighbors(x - 1, y);
        }

        if (isOpen(x, y + 1) && value < getNumber(x, y + 1)) {
            setNumber(x, y + 1, value + 1);
            changes = fillNeighbors(x, y + 1);
        }

        if (isOpen(x + 1, y) && value < getNumber(x + 1, y)) {
            setNumber(x + 1, y, value + 1);
            changes = fillNeighbors(x + 1, y);
        }
        return changes;
    }

    private boolean isOpen0(int x, int y) {
        return Long.bitCount(f(x, y) + seed) % 2 == 0;
    }

    private static long f(int x, int y) {
        // x*x + 3*x + 2*x*y + y + y*y
        return (long) x * x + 3L * x + 2L * x * y + y + (long) y * y;
    }
}
