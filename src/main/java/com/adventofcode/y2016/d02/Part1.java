package com.adventofcode.y2016.d02;

import java.util.List;

public class Part1 {
    static void part1(List<String> input) {
        int current = 5;
        for (String line : input) {
            for (char c : line.toCharArray()) {
                current = move(current, c);
            }
            System.out.print(current);
        }
        System.out.println();
    }

    private static int move(int curr, char dir) {
        return switch (dir) {
            case 'U' -> moveUp(curr);
            case 'D' -> moveDown(curr);
            case 'L' -> moveLeft(curr);
            case 'R' -> moveRight(curr);
            default -> curr;
        };
    }

    private static int moveUp(int curr) {
        return switch (curr) {
            case 4, 5, 6, 7, 8, 9 -> curr - 3;
            default -> curr;
        };
    }

    private static int moveDown(int curr) {
        return switch (curr) {
            case 1, 2, 3, 4, 5, 6 -> curr + 3;
            default -> curr;
        };
    }

    private static int moveLeft(int curr) {
        return switch (curr) {
            case 2, 3, 5, 6, 8, 9 -> curr - 1;
            default -> curr;
        };
    }

    private static int moveRight(int curr) {
        return switch (curr) {
            case 1, 2, 4, 5, 7, 8 -> curr + 1;
            default -> curr;
        };
    }
}
