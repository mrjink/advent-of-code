package com.adventofcode.y2016.d02;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            Part1.part1(input);
            Part2.part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }
}
