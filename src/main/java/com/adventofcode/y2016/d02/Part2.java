package com.adventofcode.y2016.d02;

import java.util.List;

public class Part2 {
    static void part2(List<String> input) {
        char current = '5';
        for (String line : input) {
            for (char c : line.toCharArray()) {
                current = move(current, c);
            }
            System.out.print(current);
        }
        System.out.println();
    }

    private static char move(char curr, char dir) {
        return switch (dir) {
            case 'U' -> moveUp(curr);
            case 'D' -> moveDown(curr);
            case 'L' -> moveLeft(curr);
            case 'R' -> moveRight(curr);
            default -> curr;
        };
    }

    private static char moveUp(char curr) {
        return switch (curr) {
            case '3' -> '1';
            case '6' -> '2';
            case '7' -> '3';
            case '8' -> '4';
            case 'A' -> '6';
            case 'B' -> '7';
            case 'C' -> '8';
            case 'D' -> 'B';
            default -> curr;
        };
    }

    private static char moveDown(char curr) {
        return switch (curr) {
            case '1' -> '3';
            case '2' -> '6';
            case '3' -> '7';
            case '4' -> '8';
            case '6' -> 'A';
            case '7' -> 'B';
            case '8' -> 'C';
            case 'B' -> 'D';
            default -> curr;
        };
    }

    private static char moveLeft(char curr) {
        return switch (curr) {
            case '3' -> '2';
            case '4' -> '3';
            case '6' -> '5';
            case '7' -> '6';
            case '8' -> '7';
            case '9' -> '8';
            case 'B' -> 'A';
            case 'C' -> 'B';
            default -> curr;
        };
    }

    private static char moveRight(char curr) {
        return switch (curr) {
            case '2' -> '3';
            case '3' -> '4';
            case '5' -> '6';
            case '6' -> '7';
            case '7' -> '8';
            case '8' -> '9';
            case 'A' -> 'B';
            case 'B' -> 'C';
            default -> curr;
        };
    }
}
