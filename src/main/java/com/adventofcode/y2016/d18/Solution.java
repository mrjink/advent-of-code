package com.adventofcode.y2016.d18;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            List<BitSet> map = getMap(line, 40);
            for (BitSet bitSet : map) {
                System.out.println(fromBitSet(bitSet, line.length()));
            }
            System.out.println(getSafeTiles(map, line.length()));
            System.out.println();
        }
    }

    private static void part2(List<String> input) {
        for (String line : input) {
            List<BitSet> map = getMap(line, 400000);
            System.out.println(getSafeTiles(map, line.length()));
        }
    }

    private static int getSafeTiles(List<BitSet> map, int length) {
        int sum = 0;
        for (BitSet bitSet : map) {
            for (int i = 1; i <= length; i++) {
                if (!bitSet.get(i)) {
                    sum++;
                }
            }
        }
        return sum;
    }

    private static List<BitSet> getMap(String line, int size) {
        List<BitSet> map = new ArrayList<>();
        map.add(toBitSet(line));
        for (int i = 1; i < size; i++) {
            map.add(nextBitSet(map.get(i - 1), line.length()));
        }
        return map;
    }

    private static String fromBitSet(BitSet bitSet, int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= length; i++) {
            stringBuilder.append(bitSet.get(i) ? "^" : ".");
        }
        return stringBuilder.toString();
    }

    private static BitSet nextBitSet(BitSet bitSet, int length) {
        BitSet next = new BitSet();
        for (int i = 1; i <= length; i++) {
            boolean left = bitSet.get(i - 1);
            boolean center = bitSet.get(i);
            boolean right = bitSet.get(i + 1);
            // center doesn't actually matter here?
            if ((left && center && !right) || (!left && center && right)
                || (left && !center && !right) || (!left && !center && right)) {
                next.set(i);
            }
        }
        return next;
    }

    private static BitSet toBitSet(String input) {
        BitSet bitSet = new BitSet();
        int length = input.length();
        for (int i = 1; i <= length; i++) {
            if (input.charAt(i - 1) == '^') {
                bitSet.set(i);
            }
        }
        return bitSet;
    }
}
