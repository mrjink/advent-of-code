package com.adventofcode.y2016.d12.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

import java.util.function.IntUnaryOperator;

public class CopyRegisterInstruction extends Instruction {
    private final Register register1;

    public CopyRegisterInstruction(Register register1, Register register2) {
        super(register2);
        this.register1 = register1;
    }

    @Override
    protected IntUnaryOperator operation() {
        return operand -> register1.getValue();
    }

    @Override
    public String toString() {
        return "cpy %s %s".formatted(register1.getName(), register.getName());
    }
}
