package com.adventofcode.y2016.d12.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

public class JumpValueNotZeroInstruction extends Instruction {
    private final int value;
    private final int offset;

    public JumpValueNotZeroInstruction(int value, int offset) {
        super(null);
        this.value = value;
        this.offset = offset;
    }

    @Override
    protected int jump(Register register) {
        if (value != 0) {
            return offset;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "jnz %d %d".formatted(value, offset);
    }
}
