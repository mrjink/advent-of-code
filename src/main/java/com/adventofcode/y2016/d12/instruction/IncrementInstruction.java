package com.adventofcode.y2016.d12.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

import java.util.function.IntUnaryOperator;

public class IncrementInstruction extends Instruction {
    public IncrementInstruction(Register register) {
        super(register);
    }

    @Override
    protected IntUnaryOperator operation() {
        return operand -> operand + 1;
    }

    @Override
    public String toString() {
        return "inc %s".formatted(register.getName());
    }
}
