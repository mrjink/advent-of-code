package com.adventofcode.y2016.d12.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

import java.util.function.IntUnaryOperator;

public class CopyValueInstruction extends Instruction {
    private final int value;

    public CopyValueInstruction(int value, Register register) {
        super(register);
        this.value = value;
    }

    @Override
    protected IntUnaryOperator operation() {
        return operand -> value;
    }

    @Override
    public String toString() {
        return "cpy %d %s".formatted(value, register.getName());
    }
}
