package com.adventofcode.y2016.d12.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

public class JumpRegisterNotZeroInstruction extends Instruction {
    private final int offset;

    public JumpRegisterNotZeroInstruction(Register register, int offset) {
        super(register);
        this.offset = offset;
    }

    @Override
    protected int jump(Register register) {
        if (register.getValue() != 0) {
            return offset;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "jnz %s %d".formatted(register.getName(), offset);
    }
}
