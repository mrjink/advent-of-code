package com.adventofcode.y2016.d12;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;
import com.adventofcode.util.Util;
import com.adventofcode.y2016.d12.instruction.CopyRegisterInstruction;
import com.adventofcode.y2016.d12.instruction.CopyValueInstruction;
import com.adventofcode.y2016.d12.instruction.DecrementInstruction;
import com.adventofcode.y2016.d12.instruction.IncrementInstruction;
import com.adventofcode.y2016.d12.instruction.JumpRegisterNotZeroInstruction;
import com.adventofcode.y2016.d12.instruction.JumpValueNotZeroInstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        Map<String, Register> registers = new HashMap<>();
        List<Instruction> instructions = getInstructions(input, registers);
        execute(instructions);
        System.out.println(registers.values());
    }

    private static void part2(List<String> input) {
        Map<String, Register> registers = new HashMap<>();
        registers.put("c", new Register("c", 1));
        List<Instruction> instructions = getInstructions(input, registers);
        execute(instructions);
        System.out.println(registers.values());
    }

    private static void execute(List<Instruction> instructions) {
        int pc = 0;
        do {
            pc += instructions.get(pc).execute();
        } while (pc >= 0 && pc < instructions.size());
    }

    private static List<Instruction> getInstructions(List<String> input, Map<String, Register> registers) {
        List<Instruction> instructions = new ArrayList<>();
        for (String line : input) {
            instructions.add(getInstruction(registers, line));
        }
        return instructions;
    }

    private static Instruction getInstruction(Map<String, Register> registers, String line) {
        String[] parts = line.split(" ", 2);
        String[] args = parts[1].split(" ");
        switch (parts[0]) {
            case "cpy":
                try {
                    return new CopyValueInstruction(Integer.parseInt(args[0]), registers.computeIfAbsent(args[1], Register::new));
                } catch (NumberFormatException e) {
                    return new CopyRegisterInstruction(registers.computeIfAbsent(args[0], Register::new), registers.computeIfAbsent(args[1], Register::new));
                }
            case "inc":
                return new IncrementInstruction(registers.computeIfAbsent(args[0], Register::new));
            case "dec":
                return new DecrementInstruction(registers.computeIfAbsent(args[0], Register::new));
            case "jnz":
                try {
                    return new JumpValueNotZeroInstruction(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
                } catch (NumberFormatException e) {
                    return new JumpRegisterNotZeroInstruction(registers.computeIfAbsent(args[0], Register::new), Integer.parseInt(args[1]));
                }
            default:
                return null;
        }
    }
}
