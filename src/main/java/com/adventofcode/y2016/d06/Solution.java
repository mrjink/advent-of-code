package com.adventofcode.y2016.d06;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int length = input.get(0).length();
        char[] message = new char[length];
        for (int i = 0; i < length; i++) {
            char c = getMax(input, i);
            message[i] = c;
        }
        System.out.println(message);
    }

    private static char getMax(List<String> input, final int i) {
        Map<Character, Long> map = input.stream().map(line -> line.charAt(i)).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Optional<Long> max = map.values().stream().max(Long::compare);
        if (max.isPresent()) {
            return map.entrySet().stream().filter(e -> e.getValue().equals(max.get())).map(Map.Entry::getKey).findFirst().orElse(' ');
        }
        return '-';
    }

    private static void part2(List<String> input) {
        int length = input.get(0).length();
        char[] message = new char[length];
        for (int i = 0; i < length; i++) {
            char c = getMin(input, i);
            message[i] = c;
        }
        System.out.println(message);
    }

    private static char getMin(List<String> input, final int i) {
        Map<Character, Long> map = input.stream().map(line -> line.charAt(i)).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Optional<Long> max = map.values().stream().min(Long::compare);
        if (max.isPresent()) {
            return map.entrySet().stream().filter(e -> e.getValue().equals(max.get())).map(Map.Entry::getKey).findFirst().orElse(' ');
        }
        return '-';
    }

}
