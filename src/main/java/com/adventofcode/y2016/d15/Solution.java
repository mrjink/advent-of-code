package com.adventofcode.y2016.d15;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final Pattern DISC_PATTERN = Pattern.compile("Disc #(\\d+) has (\\d+) positions?; at time=0, it is at position (\\d+)\\.");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            solve(getDiscs(input));

            input = Util.readAllLines(Solution.class, "input2");
            solve(getDiscs(input));
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Disc> getDiscs(List<String> input) {
        List<Disc> discs = new ArrayList<>();
        for (String line : input) {
            Matcher matcher = DISC_PATTERN.matcher(line);
            if (matcher.find()) {
                discs.add(new Disc(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3))));
            }
        }
        return discs;
    }

    private static void solve(List<Disc> discs) {
        System.out.println(discs);
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int pass = 0;
            for (Disc disc : discs) {
                if (disc.pass(i)) {
                    pass++;
                }
            }
            if (pass == discs.size()) {
                System.out.println(i);
                break;
            }
        }
    }

    private record Disc(int number, int modulo, int start) {
        private boolean pass(int startTime) {
            return (startTime + number + start) % modulo == 0;
        }
    }
}
