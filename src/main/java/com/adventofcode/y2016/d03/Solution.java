package com.adventofcode.y2016.d03;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int count = 0;
        for (String line : input) {
            int[] sides = Arrays.stream(line.trim().split(" +")).mapToInt(Integer::parseInt).toArray();
            if (isPossible(sides)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static void part2(List<String> input) {
        int count = 0;
        for (int i = 0; i < input.size(); i += 3) {
            String line0 = input.get(i);
            int[] sides0 = Arrays.stream(line0.trim().split(" +")).mapToInt(Integer::parseInt).toArray();
            String line1 = input.get(i + 1);
            int[] sides1 = Arrays.stream(line1.trim().split(" +")).mapToInt(Integer::parseInt).toArray();
            String line2 = input.get(i + 2);
            int[] sides2 = Arrays.stream(line2.trim().split(" +")).mapToInt(Integer::parseInt).toArray();

            if (isPossible(sides0[0], sides1[0], sides2[0])) {
                count++;
            }
            if (isPossible(sides0[1], sides1[1], sides2[1])) {
                count++;
            }
            if (isPossible(sides0[2], sides1[2], sides2[2])) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean isPossible(int... ints) {
        int total = IntStream.of(ints).sum();
        int max = IntStream.of(ints).max().orElse(-1);
        // System.out.printf("%s (%d, %d) = %s%n", Arrays.toString(ints), total, max, total - max > max);
        return total - max > max;
    }
}
