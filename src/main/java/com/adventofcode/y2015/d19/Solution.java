package com.adventofcode.y2015.d19;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern PATTERN = Pattern.compile("^([^ ]+) => ([^ ]+)$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Replacement> replacements = new ArrayList<>();
            String molecule = "";
            for (String line : input) {
                Matcher matcher = PATTERN.matcher(line);
                if (matcher.find()) {
                    replacements.add(new Replacement(matcher.group(1), matcher.group(2)));
                }
                if (line.isBlank()) {
                    continue;
                }
                molecule = line;
            }

            System.out.println(replacements);
            System.out.println(molecule);

            part1(molecule, replacements);
            part2(molecule, replacements);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String molecule, List<Replacement> replacements) {
        Collection<String> molecules = new HashSet<>();
        for (Replacement replacement : replacements) {
            int idx = -1;
            while ((idx = molecule.indexOf(replacement.from, idx + 1)) != -1) {
                molecules.add(molecule.substring(0, idx) + replacement.to + molecule.substring(idx + replacement.from.length()));
            }
        }
        System.out.println(molecules.size());
    }

    private static void part2(String target, List<Replacement> replacements) {
        Pattern pattern = Pattern.compile(replacements.stream()
                .map(Replacement::to)
                .sorted(Comparator.comparingInt(String::length).reversed())
                .collect(Collectors.joining("|", "(", ")")));
        System.out.println(pattern);
        Map<String, String> map = replacements.stream().collect(Collectors.groupingBy(Replacement::to, Collectors.mapping(Replacement::from, Collectors.joining(""))));
        System.out.println(map);
        AtomicInteger count = new AtomicInteger(0);
        do {
            Matcher matcher = pattern.matcher(target);
            if (!matcher.find()) {
                break;
            }
            // FIXME: this doesn't work for the test_input.  replaceFirst works there, but not with the real input. :(
            target = matcher.replaceAll(mr -> {
                count.incrementAndGet();
                return map.get(mr.group(1));
            });
        } while (!target.equals("e"));
        System.out.println(count);
    }

    private record Replacement(String from, String to) {
        @Override
        public String toString() {
            return from + " => " + to;
        }
    }
}
