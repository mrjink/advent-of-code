package com.adventofcode.y2015.d15;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ToLongFunction;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Ingredient> ingredients = getIngredients(input);
            part1(ingredients);
            part2(ingredients);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Ingredient> getIngredients(List<String> input) {
        List<Ingredient> ingredients = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split("[:, ]+");
            ingredients.add(new Ingredient(split[0], Integer.parseInt(split[2]), Integer.parseInt(split[4]),
                    Integer.parseInt(split[6]), Integer.parseInt(split[8]), Integer.parseInt(split[10])));
        }
        ingredients.forEach(System.out::println);
        return ingredients;
    }

    private static long blup(List<Ingredient> ingredients, ToLongFunction<Map<Ingredient, Long>> function) {
        Map<Ingredient, Long> result = new HashMap<>();
        return blup(result, ingredients, 100, 0, function);
    }

    private static long blup(Map<Ingredient, Long> result, List<Ingredient> ingredients, long remainder, long maxScore, ToLongFunction<Map<Ingredient, Long>> function) {
        if (ingredients.size() == 1) {
            result.put(ingredients.get(0), remainder);
            return Math.max(maxScore, function.applyAsLong(result));
        }
        long score = maxScore;
        for (long i = 0; i <= remainder; i++) {
            result.put(ingredients.get(0), i);
            score = Math.max(score, blup(result, ingredients.subList(1, ingredients.size()), remainder - i, score, function));
        }
        return score;
    }

    private static long calcScore1(Map<Ingredient, Long> result) {
        long capacity = Math.max(0, result.entrySet().stream().mapToLong(e -> e.getKey().capacity * e.getValue()).sum());
        long durability = Math.max(0, result.entrySet().stream().mapToLong(e -> e.getKey().durability * e.getValue()).sum());
        long flavor = Math.max(0, result.entrySet().stream().mapToLong(e -> e.getKey().flavor * e.getValue()).sum());
        long texture = Math.max(0, result.entrySet().stream().mapToLong(e -> e.getKey().texture * e.getValue()).sum());
        return capacity * durability * flavor * texture;
    }

    private static long calcScore2(Map<Ingredient, Long> result) {
        long calories = Math.max(0, result.entrySet().stream().mapToLong(e -> e.getKey().calories * e.getValue()).sum());
        if (calories == 500) {
            return calcScore1(result);
        }
        return 0;
    }

    private static void part1(List<Ingredient> ingredients) {
        System.out.println(blup(ingredients, Solution::calcScore1));
    }

    private static void part2(List<Ingredient> ingredients) {
        System.out.println(blup(ingredients, Solution::calcScore2));
    }

    private record Ingredient(String name, int capacity, int durability, int flavor, int texture, int calories) {
    }
}
