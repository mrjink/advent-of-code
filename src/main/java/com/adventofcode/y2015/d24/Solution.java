package com.adventofcode.y2015.d24;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Integer> ints = input.stream().map(Integer::parseInt).toList();
            part1(ints);
            part2(ints);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<Integer> ints) {
        solve(ints, 3);
    }

    private static void part2(List<Integer> ints) {
        solve(ints, 4);
    }

    private static void solve(List<Integer> ints, int parts) {
        int target = ints.stream().mapToInt(i -> i).sum() / parts;
        System.out.printf("Trying to find %d.%n", target);
        List<List<Integer>> groups = findGroups(ints, target);
        Map<Integer, Long> sizeMap = groups.stream().collect(Collectors.groupingBy(List::size, Collectors.counting()));
        // System.out.println(sizeMap);
        int smallestGroup = sizeMap.keySet().stream().mapToInt(i -> i).min().orElse(-1);
        long minEntanglement = groups.stream()
                .filter(l -> l.size() == smallestGroup)
                .mapToLong(Solution::quantumEntanglement)
                .min()
                .orElse(-1L);
        System.out.println(minEntanglement);
    }

    private static long quantumEntanglement(List<Integer> ints) {
        return ints.stream().mapToLong(Integer::longValue).reduce(1L, (a, b) -> a * b);
    }

    private static List<List<Integer>> findGroups(List<Integer> ints, int target) {
        List<Integer> sorted = new ArrayList<>(ints);
        sorted.sort(Comparator.reverseOrder());
        List<List<Integer>> result = new ArrayList<>();
        findGroups(target, sorted, new ArrayList<>(), result);
        return result;
    }

/*
    private static List<List<Integer>> findGroups(int[] ints) {
        List<Integer> list = new ArrayList<>();
        list.stream().mapToInt(a -> -a).sorted().map(a -> -a).boxed().toList();
        int[] sorted = IntStream.of(ints).map(a -> -a).sorted().map(a -> -a).toArray();
        int target = IntStream.of(ints).sum() / 3;
        System.out.printf("Trying to find %d.%n", target);
        List<List<Integer>> result = new ArrayList<>();
        findGroups(target, sorted, new ArrayList<>(), result);
        return result;
    }
*/

    private static void findGroups(int target, List<Integer> sorted, List<Integer> current, List<List<Integer>> result) {
        for (int i = 0; i < sorted.size(); i++) {
            int curr = sorted.get(i);
            if (curr == target) {
                List<Integer> newCurrent = new ArrayList<>(current);
                newCurrent.add(curr);
                result.add(newCurrent);
            } else if (curr <= target) {
                List<Integer> newCurrent = new ArrayList<>(current);
                newCurrent.add(curr);
                findGroups(target - curr, sorted.subList(i + 1, sorted.size()), newCurrent, result);
            }
        }
    }

/*
    private static void findGroups(int target, int[] sorted, List<Integer> current, List<List<Integer>> result) {
        for (int i = 0; i < sorted.length; i++) {
            int curr = sorted[i];
            if (curr == target) {
                List<Integer> newCurrent = new ArrayList<>(current);
                newCurrent.add(curr);
                result.add(newCurrent);
            } else if (curr <= target) {
                List<Integer> newCurrent = new ArrayList<>(current);
                newCurrent.add(curr);
                findGroups(target - curr, Arrays.copyOfRange(sorted, i + 1, sorted.length), newCurrent, result);
            }
        }
    }
*/
}
