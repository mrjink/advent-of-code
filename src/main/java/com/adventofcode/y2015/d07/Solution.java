package com.adventofcode.y2015.d07;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        Map<String, Gate> memory = getMemory(input);
        print(memory);

        int value = memory.get("a").getValue();
        System.out.println("a: " + value);
    }

    private static void part2(List<String> input) {
        Map<String, Gate> memory = getMemory(input);
        memory.put("b", new ValueGate(memory, 956));
        print(memory);

        int value = memory.get("a").getValue();
        System.out.println("a: " + value);
    }

    private static Map<String, Gate> getMemory(List<String> input) {
        Map<String, Gate> memory = new TreeMap<>();
        AbstractGate.clearCache();

        for (String line : input) {
            String[] operands = line.split(" -> ", 2);
            String target = operands[1];
            Gate gate = getGate(memory, operands[0]);
            memory.put(target, gate);
        }
        return memory;
    }

    private static void print(Map<String, Gate> memory) {
        memory.forEach((name, gate) -> System.out.format("%s: %s%n", name, gate.toString()));
    }

    private static Gate getGate(Map<String, Gate> memory, String operands) {
        String[] command = operands.split(" ");
        return switch (command.length) {
            case 1 -> gate(memory, command[0]);
            case 2 -> gate(memory, command[0], command[1]);
            case 3 -> gate(memory, command[0], command[1], command[2]);
            default -> throw new IllegalArgumentException(operands);
        };
    }

    private static Gate gate(Map<String, Gate> memory, String s1) {
        try {
            return new ValueGate(memory, Integer.parseInt(s1));
        } catch (NumberFormatException e) {
            return new GateGate(memory, s1);
        }
    }

    private static Gate gate(Map<String, Gate> memory, String s1, String s2) {
        if (s1.equals("NOT")) {
            return new NotGate(memory, s2);
        }
        throw new IllegalArgumentException(s1 + " " + s2);
    }

    private static Gate gate(Map<String, Gate> memory, String s1, String s2, String s3) {
        return switch (s2) {
            case "AND" -> new AndGate(memory, s1, s3);
            case "OR" -> new OrGate(memory, s1, s3);
            case "LSHIFT" -> new ShiftLeftGate(memory, s1, Integer.parseInt(s3));
            case "RSHIFT" -> new ShiftRightGate(memory, s1, Integer.parseInt(s3));
            default -> throw new IllegalArgumentException(s1 + " " + s2 + " " + s3);
        };
    }
}
