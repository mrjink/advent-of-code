package com.adventofcode.y2015.d07;

import java.util.Map;

class GateGate extends AbstractGate {
    private final String sourceGate;

    protected GateGate(Map<String, Gate> memory, String sourceGate) {
        super(memory);
        this.sourceGate = sourceGate;
    }

    @Override
    public int getValue() {
        return getValue(sourceGate);
    }

    @Override
    public String asString() {
        return sourceGate;
    }
}
