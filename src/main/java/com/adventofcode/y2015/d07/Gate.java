package com.adventofcode.y2015.d07;

interface Gate {
    Gate ZERO_GATE = new AbstractGate(null) {
        @Override
        public int getValue() {
            System.out.println("Zero gate!");
            return 0;
        }

        @Override
        public String asString() {
            return "ZeroGate";
        }
    };

    int getValue();

    String asString();
}
