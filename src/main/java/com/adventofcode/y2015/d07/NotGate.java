package com.adventofcode.y2015.d07;

import java.util.Map;

class NotGate extends AbstractGate {
    private final String sourceGate;

    NotGate(Map<String, Gate> memory, String sourceGate) {
        super(memory);
        this.sourceGate = sourceGate;
    }

    @Override
    public int getValue() {
        return 65535 ^ getValue(sourceGate);
    }

    @Override
    public String asString() {
        return "~" + sourceGate;
    }
}
