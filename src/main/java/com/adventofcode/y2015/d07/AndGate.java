package com.adventofcode.y2015.d07;

import java.util.Map;

class AndGate extends AbstractGate {
    private final String gate1;
    private final String gate2;

    AndGate(Map<String, Gate> memory, String gate1, String gate2) {
        super(memory);
        this.gate1 = gate1;
        this.gate2 = gate2;
    }

    @Override
    public int getValue() {
        return getValue(gate1) & getValue(gate2);
    }

    @Override
    public String asString() {
        return gate1 + " & " + gate2;
    }
}
