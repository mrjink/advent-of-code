package com.adventofcode.y2015.d07;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

abstract class AbstractGate implements Gate {
    private static final Map<String, Integer> VALUE_CACHE = new ConcurrentHashMap<>();
    private final Map<String, Gate> memory;

    protected AbstractGate(Map<String, Gate> memory) {
        this.memory = memory;
    }

    public static void clearCache() {
        VALUE_CACHE.clear();
    }

    protected int getValue(String gate) {
        if (VALUE_CACHE.containsKey(gate)) {
            return VALUE_CACHE.get(gate);
        }
        int valueFromMemory = getValueFromMemory(gate);
        VALUE_CACHE.put(gate, valueFromMemory);
        return valueFromMemory;
    }

    private int getValueFromMemory(String gate) {
        System.out.println("Get value for " + gate + " " + memory.getOrDefault(gate, ZERO_GATE).toString());
        try {
            // FIXME: Ugly much? This takes care of things like "1 AND x"
            return Integer.parseInt(gate);
        } catch (NumberFormatException e) {
            return memory.getOrDefault(gate, ZERO_GATE).getValue();
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + asString();
    }
}
