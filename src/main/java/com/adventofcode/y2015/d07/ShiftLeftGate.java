package com.adventofcode.y2015.d07;

import java.util.Map;

public class ShiftLeftGate extends AbstractGate {
    private final String sourceGate;
    private final int shift;

    public ShiftLeftGate(Map<String, Gate> memory, String sourceGate, int shift) {
        super(memory);
        this.sourceGate = sourceGate;
        this.shift = shift;
    }

    @Override
    public int getValue() {
        return getValue(sourceGate) << shift;
    }

    @Override
    public String asString() {
        return sourceGate + " << " + shift;
    }
}
