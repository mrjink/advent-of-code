package com.adventofcode.y2015.d07;

import java.util.Map;

class ValueGate extends AbstractGate {
    private final int value;

    ValueGate(Map<String, Gate> memory, int value) {
        super(memory);
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String asString() {
        return "" + value;
    }
}
