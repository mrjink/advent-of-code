package com.adventofcode.y2015.d05;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern DOUBLE_LETTER = Pattern.compile("([a-z])\\1");
    private static final Pattern DOUBLE_PAIR = Pattern.compile("([a-z][a-z]).*\\1");
    private static final Pattern REPETITION = Pattern.compile("([a-z])[a-z]\\1");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int count = 0;
        for (String line : input) {
            if (nice1(line)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static void part2(List<String> input) {
        int count = 0;
        for (String line : input) {
            if (nice2(line)) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean nice1(String input) {
        return !badStrings(input) && threeVowels(input) && doubleLetter(input);
    }

    private static boolean badStrings(String input) {
        return input.contains("ab") || input.contains("cd") || input.contains("pq") || input.contains("xy");
    }

    private static boolean threeVowels(String input) {
        return input.split("[aeiou]", -1).length >= 4;
    }

    private static boolean doubleLetter(String input) {
        return DOUBLE_LETTER.matcher(input).find();
    }

    private static boolean nice2(String input) {
        return doublePair(input) && repeats(input);
    }

    private static boolean doublePair(String input) {
        return DOUBLE_PAIR.matcher(input).find();
    }

    private static boolean repeats(String input) {
        return REPETITION.matcher(input).find();
    }


}
