package com.adventofcode.y2015.d10;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            part1("1113222113");
            part2("1113222113");
        } catch (Exception e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        String string = input;
        for (int i = 0; i < 40; i++) {
            string = getNext(string);
        }
        System.out.println(string);
        System.out.println(string.length());
    }

    private static void part2(String input) {
        String string = input;
        for (int i = 0; i < 50; i++) {
            string = getNext(string);
        }
        System.out.println(string);
        System.out.println(string.length());
    }

    private static String getNext(String string) {
        char c = string.charAt(0);
        int count = 1;
        StringBuilder result = new StringBuilder();
        for (int i = 1; i < string.length(); i++) {
            if (string.charAt(i) == c) {
                count++;
            } else {
                result.append(count);
                result.append(c);
                c = string.charAt(i);
                count = 1;
            }
        }
        result.append(count);
        result.append(c);
        return result.toString();
    }
}
