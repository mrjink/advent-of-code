package com.adventofcode.y2015.d23.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

public class JumpIfEvenInstruction extends Instruction {
    private final int offset;

    public JumpIfEvenInstruction(Register register, int offset) {
        super(register);
        this.offset = offset;
    }

    @Override
    protected int jump(Register register) {
        if (register.getValue() % 2 == 0) {
            return offset;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "jie %s, %+d".formatted(register.getName(), offset);
    }
}
