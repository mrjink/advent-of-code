package com.adventofcode.y2015.d23.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

import java.util.function.IntUnaryOperator;

public class HalfInstruction extends Instruction {
    public HalfInstruction(Register register) {
        super(register);
    }

    @Override
    protected IntUnaryOperator operation() {
        return operand -> operand / 2;
    }

    @Override
    public String toString() {
        return "hlf %s".formatted(register.getName());
    }
}
