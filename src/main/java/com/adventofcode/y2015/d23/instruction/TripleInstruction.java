package com.adventofcode.y2015.d23.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

import java.util.function.IntUnaryOperator;

public class TripleInstruction extends Instruction {
    public TripleInstruction(Register register) {
        super(register);
    }

    @Override
    protected IntUnaryOperator operation() {
        return operand -> operand * 3;
    }

    @Override
    public String toString() {
        return "tpl %s".formatted(register.getName());
    }
}
