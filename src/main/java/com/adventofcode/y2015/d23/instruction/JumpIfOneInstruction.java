package com.adventofcode.y2015.d23.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

public class JumpIfOneInstruction extends Instruction {
    private final int offset;

    public JumpIfOneInstruction(Register register, int offset) {
        super(register);
        this.offset = offset;
    }

    @Override
    protected int jump(Register register) {
        if (register.getValue() == 1) {
            return offset;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "jio %s, %+d".formatted(register.getName(), offset);
    }
}
