package com.adventofcode.y2015.d23.instruction;

import com.adventofcode.cpu.Instruction;
import com.adventofcode.cpu.Register;

public class JumpInstruction extends Instruction {
    private final int offset;

    public JumpInstruction(int offset) {
        super(null);
        this.offset = offset;
    }

    @Override
    protected int jump(Register register) {
        return offset;
    }

    @Override
    public String toString() {
        return "jmp %+d".formatted(offset);
    }
}
