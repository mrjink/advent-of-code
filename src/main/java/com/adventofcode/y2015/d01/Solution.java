package com.adventofcode.y2015.d01;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> test_input = Util.readAllLines(Solution.class, "test_input");
            String input = Util.readFile(Solution.class, "input");

            part1(test_input);
            part1(input);

            part2(test_input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        long floor = 0L;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            switch (c) {
                case '(' -> floor++;
                case ')' -> floor--;
            }
        }
        System.out.println(floor);
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            part1(line);
        }
    }

    private static void part2(String input) {
        long floor = 0L;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            switch (c) {
                case '(' -> floor++;
                case ')' -> {
                    floor--;
                    if (floor < 0) {
                        System.out.println(i + 1);
                        return;
                    }
                }
            }
        }
        System.out.println("?");
    }

    private static void part2(List<String> input) {
        for (String line : input) {
            part2(line);
        }
    }
}
