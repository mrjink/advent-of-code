package com.adventofcode.y2015.d18;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "test_input");
            boolean[][] lights = readLights(input);
            part1(lights, 4);
            part2(lights, 5);

            input = Util.readAllLines(Solution.class, "input");
            lights = readLights(input);
            part1(lights, 100);
            part2(lights, 100);

        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(boolean[][] lights, int steps) {
        System.out.println("Initial state:");
        print(lights);
        for (int i = 1; i <= steps; i++) {
            lights = animate(lights);
            System.out.println("After " + i + " steps:");
            print(lights);
        }
        System.out.println(countLights(lights));
    }

    private static void part2(boolean[][] lights, int steps) {
        System.out.println("Initial state:");
        corners(lights);
        print(lights);
        for (int i = 1; i <= steps; i++) {
            lights = animate(lights);
            corners(lights);
            System.out.println("After " + i + " steps:");
            print(lights);
        }
        System.out.println(countLights(lights));
    }

    private static void corners(boolean[][] lights) {
        lights[0][0] = true;
        lights[0][lights.length - 1] = true;
        lights[lights.length - 1][0] = true;
        lights[lights.length - 1][lights.length - 1] = true;
    }


    private static boolean[][] readLights(List<String> input) {
        int length = input.get(0).length();
        boolean[][] lights = new boolean[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (input.get(i).charAt(j) == '#') {
                    lights[i][j] = true;
                }
            }
        }
        return lights;
    }

    private static boolean[][] animate(boolean[][] lights) {
        boolean[][] result = new boolean[lights.length][lights.length];
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                int neighbors = neighbors(lights, i, j, true);
                if ((neighbors == 3) || (lights[i][j] && (neighbors == 2))) {
                    result[i][j] = true;
                }
            }
        }
        return result;
    }

    private static int countLights(boolean[][] lights) {
        int count = 0;
        for (boolean[] light : lights) {
            for (boolean b : light) {
                if (b) {
                    count++;
                }
            }
        }
        return count;
    }

    private static void print(boolean[][] lights) {
        for (boolean[] light : lights) {
            for (boolean b : light) {
                System.out.print(b ? "#" : ".");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static int neighbors(boolean[][] lights, int i, int j, boolean state) {
        int neighbors = 0;
        if (i > 0 && j > 0 && lights[i - 1][j - 1] == state) {
            neighbors++;
        }
        if (i > 0 && lights[i - 1][j] == state) {
            neighbors++;
        }
        if (i > 0 && j < lights.length - 1 && lights[i - 1][j + 1] == state) {
            neighbors++;
        }
        if (j > 0 && lights[i][j - 1] == state) {
            neighbors++;
        }
        if (j < lights.length - 1 && lights[i][j + 1] == state) {
            neighbors++;
        }
        if (i < lights.length - 1 && j > 0 && lights[i + 1][j - 1] == state) {
            neighbors++;
        }
        if (i < lights.length - 1 && lights[i + 1][j] == state) {
            neighbors++;
        }
        if (i < lights.length - 1 && j < lights.length - 1 && lights[i + 1][j + 1] == state) {
            neighbors++;
        }
        return neighbors;
    }
}
