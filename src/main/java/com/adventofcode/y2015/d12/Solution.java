package com.adventofcode.y2015.d12;

import com.adventofcode.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final ObjectMapper MAPPER = new ObjectMapper().findAndRegisterModules();

    public static void main(String[] args) {
        try {
            List<String> test_input = Util.readAllLines(Solution.class, "test_input");
            for (String line : test_input) {
                System.out.format("TOTAL FOR %s = %d%n", line, part1(MAPPER.readTree(line)));
                System.out.format("TOTAL FOR %s = %d%n", line, part2(MAPPER.readTree(line)));
            }

            String input = Util.readFile(Solution.class, "input");
            JsonNode jsonNode = MAPPER.readTree(input);
            System.out.println(part1(jsonNode));
            System.out.println(part2(jsonNode));
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static int part1(JsonNode root) {
        int total = root.asInt();
        for (JsonNode node : root) {
            total += part1(node);
        }
        return total;
    }

    private static int part2(JsonNode root) {
        if (isRed(root)) {
            return 0;
        }
        int total = root.asInt();
        for (JsonNode jsonNode : root) {
            total += part2(jsonNode);
        }
        return total;
    }

    private static boolean isRed(JsonNode jsonNode) {
        if (!jsonNode.isObject()) {
            return false;
        }
        Iterator<Map.Entry<String, JsonNode>> fields = jsonNode.fields();
        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> next = fields.next();
            if (next.getValue().asText().equals("red")) {
                return true;
            }
        }
        return false;
    }
}
