package com.adventofcode.y2015.d13;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final Pattern PATTERN = Pattern.compile("^([^ ]+) would (lose|gain) (0|[1-9][0-9]*) happiness units by sitting next to ([^ ]+)\\.$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        List<Happiness> happinessList = input
                .stream()
                .map(Solution::fromString)
                .collect(Collectors.toList());
        List<String> people = happinessList.stream().map(h -> h.p1).distinct().collect(Collectors.toList());
        solve(happinessList, people);
    }

    private static void part2(List<String> input) {
        List<Happiness> happinessList = input
                .stream()
                .map(Solution::fromString)
                .collect(Collectors.toList());
        List<String> people = happinessList.stream().map(h -> h.p1).distinct().collect(Collectors.toList());
        people.forEach(p -> {
            happinessList.add(new Happiness("ME", p, 0));
            happinessList.add(new Happiness(p, "ME", 0));
        });
        people.add("ME");
        solve(happinessList, people);
    }

    private static void solve(List<Happiness> happinessList, List<String> people) {
        List<List<String>> lists = Util.generatePerm(people);
        int maxHappiness = 0;
        for (List<String> list : lists) {
            int happiness = 0;
            for (int i = 0; i < list.size(); i++) {
                int j = (i + 1) % list.size();
                String p1 = list.get(i);
                String p2 = list.get(j);
                happiness += happinessList
                        .stream()
                        .filter(h -> (h.p1.equals(p1) && h.p2.equals(p2)) || (h.p1.equals(p2) && h.p2.equals(p1)))
                        .mapToInt(h -> h.units).sum();
            }
            maxHappiness = Math.max(maxHappiness, happiness);
        }
        System.out.println(happinessList);
        System.out.println(maxHappiness);
    }

    private static Happiness fromString(String string) {
        Matcher matcher = PATTERN.matcher(string);
        if (matcher.find()) {
            if (matcher.group(2).equals("gain")) {
                return new Happiness(matcher.group(1), matcher.group(4), Integer.parseInt(matcher.group(3)));
            } else {
                return new Happiness(matcher.group(1), matcher.group(4), -Integer.parseInt(matcher.group(3)));
            }
        }
        return null;
    }

    private record Happiness(String p1, String p2, int units) {
    }
}
