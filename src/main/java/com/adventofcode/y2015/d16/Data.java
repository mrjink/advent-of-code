package com.adventofcode.y2015.d16;

class Data {
    private Integer sue;
    private Integer children;
    private Integer cats;
    private Integer samoyeds;
    private Integer pomeranians;
    private Integer akitas;
    private Integer vizslas;
    private Integer goldfish;
    private Integer trees;
    private Integer cars;
    private Integer perfumes;

    public Data() {
    }

    public Data(Integer sue, Integer children, Integer cats, Integer samoyeds, Integer pomeranians, Integer akitas, Integer vizslas, Integer goldfish, Integer trees, Integer cars, Integer perfumes) {
        this.children = children;
        this.cats = cats;
        this.samoyeds = samoyeds;
        this.pomeranians = pomeranians;
        this.akitas = akitas;
        this.vizslas = vizslas;
        this.goldfish = goldfish;
        this.trees = trees;
        this.cars = cars;
        this.perfumes = perfumes;
    }

    public Integer getSue() {
        return sue;
    }

    public void setSue(Integer sue) {
        this.sue = sue;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public Integer getCats() {
        return cats;
    }

    public void setCats(Integer cats) {
        this.cats = cats;
    }

    public Integer getSamoyeds() {
        return samoyeds;
    }

    public void setSamoyeds(Integer samoyeds) {
        this.samoyeds = samoyeds;
    }

    public Integer getPomeranians() {
        return pomeranians;
    }

    public void setPomeranians(Integer pomeranians) {
        this.pomeranians = pomeranians;
    }

    public Integer getAkitas() {
        return akitas;
    }

    public void setAkitas(Integer akitas) {
        this.akitas = akitas;
    }

    public Integer getVizslas() {
        return vizslas;
    }

    public void setVizslas(Integer vizslas) {
        this.vizslas = vizslas;
    }

    public Integer getGoldfish() {
        return goldfish;
    }

    public void setGoldfish(Integer goldfish) {
        this.goldfish = goldfish;
    }

    public Integer getTrees() {
        return trees;
    }

    public void setTrees(Integer trees) {
        this.trees = trees;
    }

    public Integer getCars() {
        return cars;
    }

    public void setCars(Integer cars) {
        this.cars = cars;
    }

    public Integer getPerfumes() {
        return perfumes;
    }

    public void setPerfumes(Integer perfumes) {
        this.perfumes = perfumes;
    }

    public boolean matches1(Data data) {
        return (children == null || children.equals(data.children))
               && (cats == null || cats.equals(data.cats))
               && (samoyeds == null || samoyeds.equals(data.samoyeds))
               && (pomeranians == null || pomeranians.equals(data.pomeranians))
               && (akitas == null || akitas.equals(data.akitas))
               && (vizslas == null || vizslas.equals(data.vizslas))
               && (goldfish == null || goldfish.equals(data.goldfish))
               && (trees == null || trees.equals(data.trees))
               && (cars == null || cars.equals(data.cars))
               && (perfumes == null || perfumes.equals(data.perfumes));
    }

    public boolean matches2(Data data) {
        return (children == null || children.equals(data.children))
               && (cats == null || cats > data.cats)
               && (samoyeds == null || samoyeds.equals(data.samoyeds))
               && (pomeranians == null || pomeranians < data.pomeranians)
               && (akitas == null || akitas.equals(data.akitas))
               && (vizslas == null || vizslas.equals(data.vizslas))
               && (goldfish == null || goldfish < data.goldfish)
               && (trees == null || trees > data.trees)
               && (cars == null || cars.equals(data.cars))
               && (perfumes == null || perfumes.equals(data.perfumes));
    }
}
