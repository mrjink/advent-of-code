package com.adventofcode.y2015.d16;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Data> data = convert(input);
            part1(data);
            part2(data);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Data> convert(List<String> input) {
        List<Data> result = new ArrayList<>();
        for (String line : input) {
            String[] parts = line.split(": ", 2);
            int sue = Integer.parseInt(parts[0].substring(4));
            Data data = new Data();
            data.setSue(sue);
            String[] properties = parts[1].split(", ");
            for (String property : properties) {
                String[] kv = property.split(": ", 2);
                switch (kv[0]) {
                    case "children" -> data.setChildren(Integer.parseInt(kv[1]));
                    case "cats" -> data.setCats(Integer.parseInt(kv[1]));
                    case "samoyeds" -> data.setSamoyeds(Integer.parseInt(kv[1]));
                    case "pomeranians" -> data.setPomeranians(Integer.parseInt(kv[1]));
                    case "akitas" -> data.setAkitas(Integer.parseInt(kv[1]));
                    case "vizslas" -> data.setVizslas(Integer.parseInt(kv[1]));
                    case "goldfish" -> data.setGoldfish(Integer.parseInt(kv[1]));
                    case "trees" -> data.setTrees(Integer.parseInt(kv[1]));
                    case "cars" -> data.setCars(Integer.parseInt(kv[1]));
                    case "perfumes" -> data.setPerfumes(Integer.parseInt(kv[1]));
                }
            }
            result.add(data);
        }
        return result;
    }

    private static void part1(List<Data> data) {
        Data ticker = new Data(null, 3, 7, 2, 3, 0, 0, 5, 3, 2, 1);
        List<Data> result = data.stream().filter(d -> d.matches1(ticker)).toList();
        if (result.size() == 1) {
            System.out.println(result.get(0).getSue());
        }
    }

    private static void part2(List<Data> data) {
        Data ticker = new Data(null, 3, 7, 2, 3, 0, 0, 5, 3, 2, 1);
        List<Data> result = data.stream().filter(d -> d.matches2(ticker)).toList();
        if (result.size() == 1) {
            System.out.println(result.get(0).getSue());
        }
    }

}
