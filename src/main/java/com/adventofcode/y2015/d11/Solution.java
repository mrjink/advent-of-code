package com.adventofcode.y2015.d11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            part1("vzbxkghb");
            part1("vzbxxyzz");
        } catch (Exception e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        String next = input;
        do {
            next = addOne(next);
        } while (!isValid(next));
        System.out.println(next);
    }

    private static boolean isValid(String input) {
        return !containsDisallowed(input) && containsIncreasing(input) && containsTwoPairs(input);
    }

    private static boolean containsTwoPairs(String input) {
        for (char a = 'a'; a < 'z'; a++) {
            String aa = "" + a + a;
            if (input.contains(aa)) {
                for (char b = (char) (a + 1); b <= 'z'; b++) {
                    String bb = "" + b + b;
                    if (input.contains(bb)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static boolean containsDisallowed(String input) {
        return input.contains("i") || input.contains("o") || input.contains("l");
    }

    private static boolean containsIncreasing(String input) {
        for (char c = 'a'; c <= 'x'; c++) {
            if (input.contains("" + c + (char) (c + 1) + (char) (c + 2))) {
                return true;
            }
        }
        return false;
    }

    private static String addOne(String input) {
        int lastChar = input.length() - 1;
        if (input.charAt(lastChar) == 'z') {
            return addOne(input.substring(0, lastChar)) + 'a';
        }
        return input.substring(0, lastChar) + (char) (input.charAt(lastChar) + 1);
    }
}
