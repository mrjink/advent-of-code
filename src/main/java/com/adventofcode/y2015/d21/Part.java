package com.adventofcode.y2015.d21;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntBinaryOperator;
import java.util.function.ToIntFunction;

public class Part {
    static void run(List<Solution.Weapon> weapons, List<Solution.Armor> armors, List<Solution.Ring> rings,
                    int initial, IntBinaryOperator op,
                    ToIntFunction<List<Solution.Item>> playerWon, ToIntFunction<List<Solution.Item>> bossWon) {
        int moneySpent = initial;
        // 0 rings
        for (int i = 0; i < weapons.size(); i++) {
            for (int j = 0; j < armors.size() + 1; j++) {
                List<Solution.Item> gear = new ArrayList<>();
                gear.add(weapons.get(i));
                if (j < armors.size()) {
                    gear.add(armors.get(j));
                }
                moneySpent = op.applyAsInt(moneySpent, Part.fight(gear, playerWon, bossWon));
            }
        }
        // 1 ring
        for (int i = 0; i < weapons.size(); i++) {
            for (int j = 0; j < armors.size() + 1; j++) {
                for (int k = 0; k < rings.size(); k++) {
                    List<Solution.Item> gear = new ArrayList<>();
                    gear.add(weapons.get(i));
                    if (j < armors.size()) {
                        gear.add(armors.get(j));
                    }
                    gear.add(rings.get(k));
                    moneySpent = op.applyAsInt(moneySpent, Part.fight(gear, playerWon, bossWon));
                }
            }
        }
        // 2 rings
        for (int i = 0; i < weapons.size(); i++) {
            for (int j = 0; j < armors.size() + 1; j++) {
                for (int k = 0; k < rings.size() - 1; k++) {
                    for (int l = k + 1; l < rings.size(); l++) {
                        List<Solution.Item> gear = new ArrayList<>();
                        gear.add(weapons.get(i));
                        if (j < armors.size()) {
                            gear.add(armors.get(j));
                        }
                        gear.add(rings.get(k));
                        gear.add(rings.get(l));
                        moneySpent = op.applyAsInt(moneySpent, Part.fight(gear, playerWon, bossWon));
                    }
                }
            }
        }
        System.out.println(moneySpent);
    }

    static int fight(List<Solution.Item> gear, ToIntFunction<List<Solution.Item>> playerWon, ToIntFunction<List<Solution.Item>> bossWon) {
        int damage = gear.stream().mapToInt(Solution.Item::getDamage).sum();
        int armor = gear.stream().mapToInt(Solution.Item::getArmor).sum();
        Solution.Player player = new Solution.Player(100, damage, armor);
        Solution.Boss boss = new Solution.Boss(109, 8, 2);
        boolean outcome = fight(player, boss);
        if (outcome) {
            return playerWon.applyAsInt(gear);
        }
        return bossWon.applyAsInt(gear);
    }

    private static boolean fight(Solution.Player player, Solution.Boss boss) {
        int playerHP = player.hp();
        int bossHP = boss.hp();
        boolean turn = true;
        do {
            if (turn) {
                bossHP -= Math.max(1, player.damage() - boss.armor());
            } else {
                playerHP -= Math.max(1, boss.damage() - player.armor());
            }
            turn = !turn;
        } while (playerHP > 0 && bossHP > 0);
        return playerHP > 0;
    }
}
