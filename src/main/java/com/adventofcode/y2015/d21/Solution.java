package com.adventofcode.y2015.d21;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        List<Weapon> weapons = getShopItems().stream().filter(i -> i instanceof Weapon).map(i -> (Weapon) i).toList();
        List<Armor> armors = getShopItems().stream().filter(i -> i instanceof Armor).map(i -> (Armor) i).toList();
        List<Ring> rings = getShopItems().stream().filter(i -> i instanceof Ring).map(i -> (Ring) i).toList();

        Part.run(weapons, armors, rings, Integer.MAX_VALUE, Math::min, g -> g.stream().mapToInt(Item::getCost).sum(), g -> Integer.MAX_VALUE);
        Part.run(weapons, armors, rings, Integer.MIN_VALUE, Math::max, g -> Integer.MIN_VALUE, g -> g.stream().mapToInt(Item::getCost).sum());
    }

    private static List<Item> getShopItems() {
        List<Item> items = new ArrayList<>();
        items.add(new Weapon("Dagger", 8, 4, 0));
        items.add(new Weapon("Shortsword", 10, 5, 0));
        items.add(new Weapon("Warhammer", 25, 6, 0));
        items.add(new Weapon("Longsword", 40, 7, 0));
        items.add(new Weapon("Greataxe", 74, 8, 0));

        items.add(new Armor("Leather", 13, 0, 1));
        items.add(new Armor("Chainmail", 31, 0, 2));
        items.add(new Armor("Splintmail", 53, 0, 3));
        items.add(new Armor("Bandedmail", 75, 0, 4));
        items.add(new Armor("Platemail", 102, 0, 5));

        items.add(new Ring("Damage +1", 25, 1, 0));
        items.add(new Ring("Damage +2", 50, 2, 0));
        items.add(new Ring("Damage +3", 100, 3, 0));
        items.add(new Ring("Defense +1", 20, 0, 1));
        items.add(new Ring("Defense +2", 40, 0, 2));
        items.add(new Ring("Defense +3", 80, 0, 3));
        return items;
    }

    public record Boss(int hp, int damage, int armor) {
    }

    public record Player(int hp, int damage, int armor) {
    }

    public static class Item {
        private final String name;
        private final int cost;
        private final int damage;
        private final int armor;

        private Item(String name, int cost, int damage, int armor) {
            this.name = name;
            this.cost = cost;
            this.damage = damage;
            this.armor = armor;
        }

        public String getName() {
            return name;
        }

        public int getCost() {
            return cost;
        }

        public int getDamage() {
            return damage;
        }

        public int getArmor() {
            return armor;
        }
    }

    public static class Weapon extends Item {
        private Weapon(String name, int cost, int damage, int armor) {
            super(name, cost, damage, armor);
        }
    }

    public static class Armor extends Item {
        public Armor(String name, int cost, int damage, int armor) {
            super(name, cost, damage, armor);
        }
    }

    public static class Ring extends Item {
        public Ring(String name, int cost, int damage, int armor) {
            super(name, cost, damage, armor);
        }
    }
}
