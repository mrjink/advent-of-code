package com.adventofcode.y2015.d02;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int total = 0;
        for (String line : input) {
            total += part1(line);
        }
        System.out.println("Total: " + total);
    }

    private static int part1(String line) {
        int[] sides = Arrays.stream(line.split("x")).mapToInt(Integer::parseInt).sorted().toArray();
        int a = sides[0];
        int b = sides[1];
        int c = sides[2];
        int wrapping = 3 * a * b + 2 * a * c + 2 * b * c;
        System.out.println("Wrapping " + line + " with " + wrapping + " square feet.");
        return wrapping;
    }

    private static void part2(List<String> input) {
        int ribbon = 0;
        for (String line : input) {
            ribbon += part2(line);
        }
        System.out.println("Total: " + ribbon);
    }

    private static int part2(String line) {
        int[] sides = Arrays.stream(line.split("x")).mapToInt(Integer::parseInt).sorted().toArray();
        int a = sides[0];
        int b = sides[1];
        int c = sides[2];
        int ribbon = 2 * a + 2 * b + a * b * c;
        System.out.println("Tying " + line + " with " + ribbon + " feet of ribbon.");
        return ribbon;
    }
}
