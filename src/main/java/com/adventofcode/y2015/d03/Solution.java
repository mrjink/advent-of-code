package com.adventofcode.y2015.d03;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
//            List<String> test_input = Util.readAllLines(Solution.class, "test_input");
            String input = Util.readFile(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        for (String line : input) {
            part1(line);
        }
    }

    private static void part1(String input) {
        Map<House, Long> collect = getHouses(input)
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(collect.size());
    }

    private static List<House> getHouses(String input) {
        return getHouses(input, 0, 1);
    }

    private static List<House> getHouses(String input, int start, int step) {
        int x = 0;
        int y = 0;
        List<House> houses = new ArrayList<>();
        houses.add(new House(x, y));
        for (int i = start; i < input.length(); i += step) {
            char c = input.charAt(i);
            switch (c) {
                case '<' -> x--;
                case '>' -> x++;
                case '^' -> y--;
                case 'v' -> y++;
            }
            houses.add(new House(x, y));
        }
        System.out.println(houses);
        return houses;

    }

    private static void part2(List<String> input) {
        for (String line : input) {
            part2(line);
        }
    }

    private static void part2(String input) {
        List<House> houses = new ArrayList<>();
        houses.addAll(getHouses(input, 0, 2));
        houses.addAll(getHouses(input, 1, 2));
        Map<House, Long> collect = houses.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(collect.size());
    }

    private record House(int x, int y) {
    }
}
