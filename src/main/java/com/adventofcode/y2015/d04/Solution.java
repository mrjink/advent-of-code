package com.adventofcode.y2015.d04;

import jakarta.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            part1("abcdef");
            part1("pqrstuv");
            part1("ckczppom");

            part2("abcdef");
            part2("pqrstuv");
            part2("ckczppom");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) throws NoSuchAlgorithmException {
        long x = 1L;
        while (true) {
            String string = input + x;
            String md5 = md5(string);
            if (md5.startsWith("00000")) {
                System.out.format("%d %s %s%n", x, string, md5);
                return;
            }
            x++;
        }
    }

    private static String md5(String string) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(string.getBytes());
        return DatatypeConverter.printHexBinary(md5.digest());
    }

    private static void part2(String input) throws NoSuchAlgorithmException {
        long x = 1L;
        while (true) {
            String string = input + x;
            String md5 = md5(string);
            if (md5.startsWith("000000")) {
                System.out.format("%d %s %s%n", x, string, md5);
                return;
            }
            x++;
        }
    }
}
