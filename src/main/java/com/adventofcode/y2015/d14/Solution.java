package com.adventofcode.y2015.d14;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern PATTERN = Pattern.compile("([^ ]+) can fly ([0-9]+) km/s for ([0-9]+) seconds, but then must rest for ([0-9]+) seconds\\.");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Reindeer> reindeer = fromInput(input);
            part1(reindeer);
            part2(reindeer);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<Reindeer> reindeer) {
        int seconds = 2503;
        reindeer.forEach(r -> System.out.println(r + " " + r.getDistance(seconds)));
        System.out.println(reindeer.stream().mapToInt(r -> r.getDistance(seconds)).max().orElse(0));
    }

    private static void part2(List<Reindeer> reindeer) {
        int seconds = 2503;
        Map<String, Integer> points = new TreeMap<>();
        for (int i = 1; i <= seconds; i++) {
            int finalI = i;
            Map<Reindeer, Integer> collect = reindeer.stream().collect(Collectors.toMap(Function.identity(), r -> r.getDistance(finalI)));
            final int max = collect.values().stream().mapToInt(Integer::intValue).max().orElse(0);
            collect.forEach((r, d) -> {
                if (d == max) {
                    points.compute(r.name, (k, v) -> (v == null) ? 1 : (v + 1));
                }
            });
        }
        System.out.println(points);
    }

    private static List<Reindeer> fromInput(List<String> input) {
        List<Reindeer> reindeer = new ArrayList<>();
        for (String line : input) {
            Matcher matcher = PATTERN.matcher(line);
            if (matcher.find()) {
                reindeer.add(new Reindeer(matcher.group(1),
                        Integer.parseInt(matcher.group(2)),
                        Integer.parseInt(matcher.group(3)),
                        Integer.parseInt(matcher.group(4))));
            } else {
                System.out.println("Can't parse " + line);
            }
        }
        return reindeer;
    }

    private record Reindeer(String name, int speed, int stamina, int rest) {
        public int getDistance(int seconds) {
            if (seconds <= 0) {
                return 0;
            }
            int cycle = stamina + rest;
            return ((seconds / cycle) * stamina * speed) + (Math.min(stamina, seconds % cycle) * speed);
        }
    }
}
