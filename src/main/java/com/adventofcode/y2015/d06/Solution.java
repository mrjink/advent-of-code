package com.adventofcode.y2015.d06;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        int[][] lights = new int[1000][1000];

        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()) {
            String command = scanner.next();
            if (command.equals("turn")) {
                command = scanner.next();
            }
            int[] from = Arrays.stream(scanner.next().split(",")).mapToInt(Integer::parseInt).toArray();
            scanner.next(); // "through"
            int[] to = Arrays.stream(scanner.next().split(",")).mapToInt(Integer::parseInt).toArray();

            IntUnaryOperator operator = switch (command) {
                case "on" -> x -> 1;
                case "off" -> x -> 0;
                case "toggle" -> x -> x ^ 1;
                default -> x -> x;
            };

            for (int y = from[1]; y <= to[1]; y++) {
                for (int x = from[0]; x <= to[0]; x++) {
                    lights[y][x] = operator.applyAsInt(lights[y][x]);
                }
            }
        }
        System.out.println(sum(lights));
    }

    private static void part2(String input) {
        int[][] lights = new int[1000][1000];

        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()) {
            String command = scanner.next();
            if (command.equals("turn")) {
                command = scanner.next();
            }
            int[] from = Arrays.stream(scanner.next().split(",")).mapToInt(Integer::parseInt).toArray();
            scanner.next(); // "through"
            int[] to = Arrays.stream(scanner.next().split(",")).mapToInt(Integer::parseInt).toArray();

            IntUnaryOperator operator = switch (command) {
                case "on" -> x -> x + 1;
                case "off" -> x -> Math.max(x - 1, 0);
                case "toggle" -> x -> x + 2;
                default -> x -> x;
            };

            for (int y = from[1]; y <= to[1]; y++) {
                for (int x = from[0]; x <= to[0]; x++) {
                    lights[y][x] = operator.applyAsInt(lights[y][x]);
                }
            }
        }
        System.out.println(sum(lights));
    }

    private static void print(int[][] lights) {
        for (int[] light : lights) {
            for (int i : light) {
                System.out.print(i == 1 ? "x" : " ");
            }
            System.out.println();
        }
    }

    private static int sum(int[][] lights) {
        int count = 0;
        for (int[] light : lights) {
            count += IntStream.of(light).sum();
        }
        return count;
    }
}
