package com.adventofcode.y2015.d25;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        part1();
    }

    private static void part1() {
        long target = find(2978, 3083);
        System.out.println(target);

        long x = 20151125L;
        for (long i = 1; i < target; i++) {
            x = next(x);
        }
        System.out.println(x);
    }

    private static long next(long x) {
        return (x * 252533L) % 33554393L;
    }

    // find a number by coordinates.
    private static long find(int x, int y) {
        long s = 1;
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            for (int j = 1; j <= i; j++) {
                if (i - j + 1 == x && j == y) {
                    return s;
                }
                s++;
            }
        }
        return -1L;
    }

    // find coordinates for a number.
    private static int[] find(long x) {
        long s = x;
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            for (int j = 1; j <= i; j++) {
                s--;
                if (s == 0) {
                    return new int[]{i - j + 1, j};
                }
            }
        }
        return new int[]{0, 0};
    }
}
