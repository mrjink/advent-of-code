package com.adventofcode.y2015.d20;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        part1(29_000_000 / 10);
        part2(29_000_000 / 11);
    }

    private static void part1(int input) {
        int[] houses = new int[input];
        int minHouse = input;
        for (int i = 1; i < input; i++) {
            for (int j = i; j < input; j += i) {
                houses[j] += i * 10;
                if (houses[j] >= input * 10 && j < minHouse) {
                    minHouse = j;
                }
            }
        }
        System.out.println(minHouse);
    }

    private static void part2(int input) {
        int[] houses = new int[input];
        int minHouse = input;
        for (int i = 1; i < input; i++) {
            for (int j = i, k = 0; j < input && k < 50; j += i, k++) {
                houses[j] += i * 11;
                if (houses[j] >= input * 11 && j < minHouse) {
                    minHouse = j;
                }
            }
        }
        System.out.println(minHouse);
    }
}
