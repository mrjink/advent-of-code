package com.adventofcode.y2015.d09;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern DISTANCES = Pattern.compile("^([^ ]+) to ([^ ]+) = ([0-9]+)$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        Set<Distance> distances = new HashSet<>();
        TreeSet<String> cities = new TreeSet<>();
        for (String line : input) {
            Matcher matcher = DISTANCES.matcher(line);
            if (matcher.find()) {
                String from = matcher.group(1);
                String to = matcher.group(2);
                int distance = Integer.parseInt(matcher.group(3));
                distances.add(new Distance(from, to, distance));
                distances.add(new Distance(to, from, distance));
                cities.add(from);
                cities.add(to);
            }
        }
        List<List<String>> permutations = Util.generatePerm(new ArrayList<>(cities));
        int minDistance = Integer.MAX_VALUE;
        for (List<String> permutation : permutations) {
            minDistance = Math.min(minDistance, getDistance(distances, permutation));
        }
        System.out.println(minDistance);
    }

    private static void part2(List<String> input) {
        Set<Distance> distances = new HashSet<>();
        TreeSet<String> cities = new TreeSet<>();
        for (String line : input) {
            Matcher matcher = DISTANCES.matcher(line);
            if (matcher.find()) {
                String from = matcher.group(1);
                String to = matcher.group(2);
                int distance = Integer.parseInt(matcher.group(3));
                distances.add(new Distance(from, to, distance));
                distances.add(new Distance(to, from, distance));
                cities.add(from);
                cities.add(to);
            }
        }
        List<List<String>> permutations = Util.generatePerm(new ArrayList<>(cities));
        int minDistance = Integer.MIN_VALUE;
        for (List<String> permutation : permutations) {
            minDistance = Math.max(minDistance, getDistance(distances, permutation));
        }
        System.out.println(minDistance);
    }

    private static int getDistance(Set<Distance> distances, List<String> cities) {
        int distance = 0;
        for (int i = 0; i < cities.size() - 1; i++) {
            distance += getDistance(distances, cities.get(i), cities.get(i + 1));
        }
        return distance;
    }

    private static int getDistance(Set<Distance> distances, String from, String to) {
        return distances
                .stream()
                .filter(d -> d.from.equals(from) && d.to.equals(to))
                .findFirst()
                .map(d -> d.distance)
                .orElseThrow(() -> new IllegalArgumentException(from + " => " + to));
    }

    private record Distance(String from, String to, int distance) {
    }
}
