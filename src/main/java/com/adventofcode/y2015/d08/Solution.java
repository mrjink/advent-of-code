package com.adventofcode.y2015.d08;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static int getLength1(String string) {
        System.out.print(string + "⇒");
        string = string.substring(1, string.length() - 1);
        string = string.replaceAll("\\\\\\\\", "/");
        string = string.replaceAll("\\\\\"", "\"");
        string = string.replaceAll("\\\\x([0-9a-f][0-9a-f])", "?");
        System.out.println(string);
        return string.length();
    }

    private static int getLength2(String string) {
        System.out.print(string + "⇒");
        string = string.replaceAll("\\\\", "\\\\\\\\");
        string = string.replaceAll("\"", "\\\\\"");
        string = '"' + string + '"';
        System.out.println(string);
        return string.length();
    }

    private static void part1(List<String> input) {
        int codeCount = 0;
        int memCount = 0;
        for (String string : input) {
            codeCount += string.length();
            memCount += getLength1(string);
        }
        System.out.format("%d − %d = %d%n", codeCount, memCount, codeCount - memCount);
    }

    private static void part2(List<String> input) {
        int codeCount = 0;
        int codeCodeCount = 0;
        for (String string : input) {
            codeCount += string.length();
            codeCodeCount += getLength2(string);
        }
        System.out.format("%d − %d = %d%n", codeCodeCount, codeCount, codeCodeCount - codeCount);
    }
}
