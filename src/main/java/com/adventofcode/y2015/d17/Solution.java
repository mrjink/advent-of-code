package com.adventofcode.y2015.d17;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.concurrent.atomic.AtomicInteger;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "test_input");
            List<Integer> ints = input.stream().map(Integer::valueOf).sorted(Collections.reverseOrder()).toList();
            part1(ints, 25);
            part2(ints, 25);


            input = Util.readAllLines(Solution.class, "input");
            ints = input.stream().map(Integer::valueOf).sorted(Collections.reverseOrder()).toList();
            part1(ints, 150);
            part2(ints, 150);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<Integer> input, int target) {
        AtomicInteger count = new AtomicInteger(0);
        getOptions(input, target, count);
        System.out.println(count);
    }

    private static void getOptions(List<Integer> input, int target, AtomicInteger count) {
        if (target == 0) {
            count.incrementAndGet();
            return;
        }
        if (input.isEmpty() || target < 0) {
            return;
        }
        for (int i = 0; i < input.size(); i++) {
            getOptions(input.subList(i + 1, input.size()), target - input.get(i), count);
        }
    }

    private static void part2(List<Integer> input, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        getOptions(input, target, 0, map);
        OptionalInt min = map.keySet().stream().mapToInt(Integer::intValue).min();
        if(min.isPresent()) {
            System.out.println(map.get(min.getAsInt()));
        }
    }

    private static void getOptions(List<Integer> input, int target, int size, Map<Integer, Integer> map) {
        if (target == 0) {
            map.merge(size, 1, Integer::sum);
            return;
        }
        if (input.isEmpty() || target < 0) {
            return;
        }
        for (int i = 0; i < input.size(); i++) {
            getOptions(input.subList(i + 1, input.size()), target - input.get(i), size + 1, map);
        }
    }

}
