package com.adventofcode.cpu;

import java.util.function.IntUnaryOperator;

public class Register {
    private final String name;
    private int value;

    public Register(String name) {
        this(name, 0);
    }

    public Register(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void operate(IntUnaryOperator operator) {
        value = operator.applyAsInt(value);
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Register[" + name + "=" + value + "]";
    }
}
