package com.adventofcode.cpu;

import java.util.function.IntUnaryOperator;

public abstract class Instruction {

    protected final Register register;

    protected Instruction(Register register) {
        this.register = register;
    }

    public int execute() {
        if (register != null) {
            register.operate(operation());
        }
        return jump(register);
    }

    protected IntUnaryOperator operation() {
        return operand -> operand;
    }

    protected int jump(Register register) {
        return 1;
    }
}
