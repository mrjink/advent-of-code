package com.adventofcode.y2021.d02;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        int position = 0;
        int depth = 0;
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()) {
            String command = scanner.next();
            int amount = scanner.nextInt();
            switch (command) {
                case "forward" -> position += amount;
                case "up" -> depth -= amount;
                case "down" -> depth += amount;
            }
        }
        System.out.format("%d × %d = %d%n", position, depth, position * depth);
    }

    private static void part2(String input) {
        int position = 0;
        int aim = 0;
        int depth = 0;
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext()) {
            String command = scanner.next();
            int amount = scanner.nextInt();
            switch (command) {
                case "forward" -> {
                    position += amount;
                    depth += amount * aim;
                }
                case "up" -> aim -= amount;
                case "down" -> aim += amount;
            }
        }
        System.out.format("%d × %d = %d%n", position, depth, position * depth);
    }
}
