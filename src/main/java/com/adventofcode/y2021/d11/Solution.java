package com.adventofcode.y2021.d11;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            int[][] octopi = Util.toArray(input);
            part1(octopi);
            octopi = Util.toArray(input);
            part2(octopi);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int[][] octopi) {
        long flashes = 0;
        for (int i = 0; i < 100; i++) {
            flashes += step(octopi);
        }
        System.out.println(flashes);
    }

    private static void part2(int[][] octopi) {
        int steps = 0;
        do {
            steps++;
        } while (step(octopi) != 100);
        System.out.println(steps);
    }

    private static void print(String prefix, int[][] array) {
        System.out.println(prefix);
        for (int[] ints : array) {
            System.out.println(Arrays.stream(ints).mapToObj(String::valueOf).collect(Collectors.joining()));
        }
        System.out.println();
    }

    private static long step(int[][] octopi) {
        int[][] flashed = new int[octopi.length][octopi[0].length];
        print("Pre-steo octopi", octopi);

        increment(octopi);
        while (flash(octopi, flashed)) ;
        afterFlash(octopi, flashed);

        print("Post-step octopi", octopi);
        print("Post-step flashed", flashed);

        return Arrays.stream(flashed).flatMapToInt(Arrays::stream).filter(x -> x > 0).count();
    }

    private static void afterFlash(int[][] octopi, int[][] flashed) {
        for (int y = 0; y < octopi.length; y++) {
            for (int x = 0; x < octopi[y].length; x++) {
                if (flashed[y][x] > 0) {
                    octopi[y][x] = 0;
                }
            }
        }
    }

    private static void increment(int[][] octopi) {
        for (int y = 0; y < octopi.length; y++) {
            for (int x = 0; x < octopi[y].length; x++) {
                octopi[y][x]++;
            }
        }
    }

    private static boolean flash(int[][] octopi, int[][] flashed) {
        boolean result = false;
        for (int y = 0; y < octopi.length; y++) {
            for (int x = 0; x < octopi[y].length; x++) {
                if (octopi[y][x] > 9 && flashed[y][x] == 0) {
                    flashed[y][x]++;
                    flash(octopi, flashed, x, y);
                    result = true;
                }
            }
        }

        return result;
    }

    private static void flash(int[][] octopi, int[][] flashed, int x, int y) {
        for (int y1 = y - 1; y1 <= y + 1; y1++) {
            if (y1 < 0 || y1 >= octopi.length) {
                continue;
            }
            for (int x1 = x - 1; x1 <= x + 1; x1++) {
                if (x1 < 0 || x1 >= octopi[y1].length) {
                    continue;
                }
                if (flashed[y1][x1] == 0) {
                    octopi[y1][x1]++;
                }
            }
        }
    }
}
