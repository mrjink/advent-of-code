package com.adventofcode.y2021.d08;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Entry> entries = getEntries(input);
            entries.forEach(System.out::println);
            part1(entries);
            part2(entries);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Entry> getEntries(List<String> input) {
        List<Entry> entries = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split(" \\| ");
            entries.add(getEntry(split[0], split[1]));
        }
        return entries;
    }

    private static Entry getEntry(String in, String out) {
        return new Entry(Arrays.stream(in.split(" ")).map(Solution::getDigit).toList(),
                Arrays.stream(out.split(" ")).map(Solution::getDigit).toList());
    }

    private static List<String> getDigit(String input) {
        return Arrays.stream(input.split("")).sorted().collect(Collectors.toList());
    }

    private static void part1(List<Entry> entries) {
        Predicate<List<String>> stringPredicate = o -> o.size() == 2 || o.size() == 3 || o.size() == 4 || o.size() == 7;
        long sum = entries
                .stream()
                .mapToLong(e -> e.output
                        .stream()
                        .filter(stringPredicate)
                        .count())
                .sum();
        System.out.println(sum);
    }

    private static void part2(List<Entry> entries) {
        int sum = 0;
        for (Entry entry : entries) {
            List<List<String>> input = entry.input;
            List<String> d1 = input.stream().filter(i -> i.size() == 2).findFirst().orElseThrow();
            List<String> d4 = input.stream().filter(i -> i.size() == 4).findFirst().orElseThrow();
            List<String> d7 = input.stream().filter(i -> i.size() == 3).findFirst().orElseThrow();

            Map<String, Long> collect = input.stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            List<String> copy = new ArrayList<>(d7);
            copy.removeAll(d1);
            String sa = copy.get(0);
            String sb = collect.entrySet().stream().filter(e -> e.getValue() == 6L).map(Map.Entry::getKey).findFirst().orElseThrow();
            String sc = collect.entrySet().stream().filter(e -> !e.getKey().equals(sa) && e.getValue() == 8L).map(Map.Entry::getKey).findFirst().orElseThrow();
            String se = collect.entrySet().stream().filter(e -> e.getValue() == 4L).map(Map.Entry::getKey).findFirst().orElseThrow();
            String sf = collect.entrySet().stream().filter(e -> e.getValue() == 9L).map(Map.Entry::getKey).findFirst().orElseThrow();
            copy = new ArrayList<>(d4);
            copy.removeAll(List.of(sb, sc, sf));
            String sd = copy.get(0);
            String sg = collect.entrySet().stream().filter(e -> !e.getKey().equals(sd) && e.getValue() == 7L).map(Map.Entry::getKey).findFirst().orElseThrow();

            System.out.format("%s%s%s%s%s%s%s%n", sa, sb, sc, sd, se, sf, sg);

            List<String> n0 = Stream.of(sa, sb, sc, se, sf, sg).sorted().toList();
            List<String> n1 = Stream.of(sc, sf).sorted().toList();
            List<String> n2 = Stream.of(sa, sc, sd, se, sg).sorted().toList();
            List<String> n3 = Stream.of(sa, sc, sd, sf, sg).sorted().toList();
            List<String> n4 = Stream.of(sb, sc, sd, sf).sorted().toList();
            List<String> n5 = Stream.of(sa, sb, sd, sf, sg).sorted().toList();
            List<String> n6 = Stream.of(sa, sb, sd, se, sf, sg).sorted().toList();
            List<String> n7 = Stream.of(sa, sc, sf).sorted().toList();
            List<String> n8 = Stream.of(sa, sb, sc, sd, se, sf, sg).sorted().toList();
            List<String> n9 = Stream.of(sa, sb, sc, sd, sf, sg).sorted().toList();

            int num = 0;
            for (List<String> strings : entry.output) {
                if (strings.equals(n0)) {
                    num = num * 10;
                }
                if (strings.equals(n1)) {
                    num = num * 10 + 1;
                }
                if (strings.equals(n2)) {
                    num = num * 10 + 2;
                }
                if (strings.equals(n3)) {
                    num = num * 10 + 3;
                }
                if (strings.equals(n4)) {
                    num = num * 10 + 4;
                }
                if (strings.equals(n5)) {
                    num = num * 10 + 5;
                }
                if (strings.equals(n6)) {
                    num = num * 10 + 6;
                }
                if (strings.equals(n7)) {
                    num = num * 10 + 7;
                }
                if (strings.equals(n8)) {
                    num = num * 10 + 8;
                }
                if (strings.equals(n9)) {
                    num = num * 10 + 9;
                }
            }
            sum += num;
        }
        System.out.println(sum);
    }

    private record Entry(List<List<String>> input, List<List<String>> output) {
    }
}
