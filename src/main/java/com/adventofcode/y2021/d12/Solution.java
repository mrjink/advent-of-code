package com.adventofcode.y2021.d12;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Path> paths = getPaths(input);
            // paths.forEach(System.out::println);
            part1(paths);
            part2(paths);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<Path> paths) {
        Cave start = findStart(paths);
        Cave end = findEnd(paths);
        int count = findPaths(new LinkedList<>(), paths, start, end,
                (p, c) -> !c.big() && p.contains(c));
        System.out.println(count);
    }

    private static void part2(List<Path> paths) {
        Cave start = findStart(paths);
        Cave end = findEnd(paths);
        int count = findPaths(new LinkedList<>(), paths, start, end, (p, c) -> {
            if (c.big()) {
                return false;
            }
            Map<String, Long> counts = p.stream()
                    .filter(x -> !x.big())
                    .collect(Collectors.groupingBy(x -> x.name, Collectors.counting()));
            long cCount = counts.getOrDefault(c.name, 0L);
            return cCount == 2L || (cCount == 1L && counts.containsValue(2L));
        });
        System.out.println(count);
    }

    private static List<Path> getPaths(List<String> input) {
        List<Path> paths = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split("-");
            String name1 = split[0];
            String name2 = split[1];
            Cave cave1 = new Cave(name1);
            Cave cave2 = new Cave(name2);
            if (!name2.equals("start") && !name1.equals("end")) {
                paths.add(new Path(cave1, cave2));
            }
            if (!name1.equals("start") && !name2.equals("end")) {
                paths.add(new Path(cave2, cave1));
            }
        }
        return paths;
    }

    private static List<Cave> findNext(List<Path> paths, Cave cave) {
        return paths.stream()
                .filter(p -> p.cave1.equals(cave))
                .map(p -> p.cave2)
                .sorted(Comparator.comparing(c -> c.name))
                .toList();
    }

    private static Cave findStart(List<Path> paths) {
        return paths.stream()
                .map(path -> path.cave1)
                .filter(cave -> cave.name.equals("start"))
                .findFirst()
                .orElseThrow();
    }

    private static Cave findEnd(List<Path> paths) {
        return paths.stream()
                .map(path -> path.cave2)
                .filter(cave -> cave.name.equals("end"))
                .findFirst()
                .orElseThrow();
    }

    private static int findPaths(LinkedList<Cave> path, List<Path> paths, Cave start, Cave end,
                                 BiPredicate<LinkedList<Cave>, Cave> stopCondition) {
        if (stopCondition.test(path, start)) {
            // System.out.format("Can't revisit the same cave: %s in %s%n", start, path);
            return 0;
        }

        path.add(start);

        if (start.equals(end)) {
            System.out.format("Found path: %s%n", path);
            path.removeLast();
            return 1;
        }

        int count = 0;
        List<Cave> nexts = findNext(paths, start);
        for (Cave next : nexts) {
            count += findPaths(path, paths, next, end, stopCondition);
        }
        path.removeLast();
        return count;
    }

    private record Cave(String name) {
        public boolean big() {
            return name.toUpperCase().equals(name);
        }

        @Override
        public String toString() {
            return name;
        }

    }

    private record Path(Cave cave1, Cave cave2) {
    }
}
