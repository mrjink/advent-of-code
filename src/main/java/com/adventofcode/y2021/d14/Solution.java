package com.adventofcode.y2021.d14;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            String polymer = input.get(0);
            List<Rule> rules = getRules(input);
            part1(polymer, rules);
            part2(polymer, rules);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Rule> getRules(List<String> input) {
        List<Rule> rules = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split(" -> ");
            if (split.length == 2) {
                rules.add(new Rule(split[0], split[1]));
            }
        }
        return rules;
    }

    private static void part1(String polymer, List<Rule> rules) {
        naiveSolve(polymer, rules, 10);
        // cleverSolve(polymer, rules, 10);
    }

    private static void part2(String polymer, List<Rule> rules) {
        cleverSolve(polymer, rules, 40);
    }

    private static void cleverSolve(String polymer, List<Rule> rules, int steps) {
        Map<String, Rule> rulesMap = rules.stream().collect(Collectors.toMap(Rule::pair, Function.identity()));
        Map<String, Long> template = new TreeMap<>();
        for (int idx = polymer.length() - 2; idx >= 0; idx--) {
            String pair = polymer.substring(idx, idx + 2);
            template.compute(pair, (s, l) -> l == null ? 1 : l + 1);
        }

        // System.out.println(template);
        for (int step = 1; step <= steps; step++) {
            Map<String, Long> copy = new TreeMap<>();
            template.forEach((k, v) -> {
                Rule rule = rulesMap.get(k);
                copy.compute(rule.new1(), (s, l) -> l == null ? v : l + v);
                copy.compute(rule.new2(), (s, l) -> l == null ? v : l + v);
            });
            template = copy;
            // System.out.println(template);
        }
        Map<String, Long> counts = new TreeMap<>();
        counts.put(polymer.substring(0, 1), 1L);
        counts.put(polymer.substring(polymer.length() - 1), 1L);
        template.forEach((k, v) -> {
            counts.compute("" + k.charAt(0), (s, l) -> l == null ? v : l + v);
            counts.compute("" + k.charAt(1), (s, l) -> l == null ? v : l + v);
        });
        // counts.forEach((k, v) -> System.out.format("%s %d%n", k, v / 2));
        long min = counts.values().stream().mapToLong(l -> l / 2).min().orElseThrow();
        long max = counts.values().stream().mapToLong(l -> l / 2).max().orElseThrow();
        System.out.println(max - min);
    }

    private static void naiveSolve(String polymer, List<Rule> rules, int steps) {
        Map<String, String> rulesMap = rules.stream().collect(Collectors.toMap(Rule::pair, Rule::replacement));
        StringBuilder template = new StringBuilder(polymer);
        for (int step = 1; step <= steps; step++) {
            for (int idx = template.length() - 2; idx >= 0; idx--) {
                String pair = template.substring(idx, idx + 2);
                template.replace(idx, idx + 2, rulesMap.get(pair));
            }
            // System.out.format("Step %2d: (%d) %s%n", step, template.length(), template);
        }
        Map<String, Long> occurrences = Arrays.stream(template.toString().split("")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        long min = occurrences.values().stream().mapToLong(l -> l).min().orElseThrow();
        long max = occurrences.values().stream().mapToLong(l -> l).max().orElseThrow();
        System.out.println(max - min);
    }

    private record Rule(String pair, String insert) {
        public String replacement() {
            return pair.charAt(0) + insert + pair.charAt(1);
        }

        public String new1() {
            return pair.charAt(0) + insert;
        }

        public String new2() {
            return insert + pair.charAt(1);
        }
    }
}
