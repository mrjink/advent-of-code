package com.adventofcode.y2021.d06;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            Map<Integer, Long> state = getState(input);
            part1(state);
            part2(state);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static Map<Integer, Long> getState(String input) {
        return Arrays.stream(input.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private static void part1(Map<Integer, Long> input) {
        solve(input, 80);
    }

    private static void part2(Map<Integer, Long> input) {
        solve(input, 256);
    }

    private static void solve(Map<Integer, Long> input, int days) {
        Map<Integer, Long> state = input;
        for (int i = 1; i <= days; i++) {
            state = nextState(state);
            System.out.format("After %2d days: %d lanternfish.%n", i, state.values().stream().mapToLong(x -> x).sum());
        }
        System.out.println(state.values().stream().mapToLong(x -> x).sum());
    }

    private static Map<Integer, Long> nextState(Map<Integer, Long> state) {
        Map<Integer, Long> nextState = new TreeMap<>();
        state.forEach((k, v) -> nextState.put(k - 1, v));
        if (nextState.containsKey(-1)) {
            Long remove = nextState.remove(-1);
            nextState.put(6, nextState.getOrDefault(6, 0L) + remove);
            nextState.put(8, remove);
        }
        return nextState;
    }
}
