package com.adventofcode.y2021.d04;

import java.util.Arrays;

class Board {
    static final int SIZE = 5;
    private final int[][] board;

    public Board(int[] numbers) {
        board = new int[SIZE][SIZE];
        if (numbers.length != SIZE * SIZE) {
            throw new IllegalArgumentException("Need " + SIZE * SIZE + " numbers to initialize board!");
        }
        for (int i = 0; i < SIZE; i++) {
            System.arraycopy(numbers, i * SIZE, board[i], 0, SIZE);
        }
    }

    public boolean mark(int x) {
        boolean result = false;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == x) {
                    board[i][j] = -x - 1;
                    result = true;
                }
            }
        }
        return result;
    }

    public boolean wins() {
        for (int i = 0; i < SIZE; i++) {
            if (board[i][0] < 0 && board[i][1] < 0 && board[i][2] < 0 && board[i][3] < 0 && board[i][4] < 0) {
                return true;
            }
            if (board[0][i] < 0 && board[1][i] < 0 && board[2][i] < 0 && board[3][i] < 0 && board[4][i] < 0) {
                return true;
            }
        }
        return false;
    }

    public void print() {
        System.out.println();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.format("%3d ", board[i][j]);
            }
            System.out.println();
        }
    }

    public int sum() {
        int sum = 0;
        for (int i = 0; i < SIZE; i++) {
            sum += Arrays.stream(board[i]).filter(x -> x > 0).sum();
        }
        return sum;
    }
}
