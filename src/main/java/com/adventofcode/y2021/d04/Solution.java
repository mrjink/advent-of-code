package com.adventofcode.y2021.d04;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            Scanner scanner = new Scanner(input);
            int[] drawn = Arrays.stream(scanner.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
            List<Board> boards = getBoards(scanner);

            System.out.println(Arrays.toString(drawn));
            boards.forEach(Board::print);

            part1(drawn, new ArrayList<>(boards));
            part2(drawn, new ArrayList<>(boards));
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int[] drawn, List<Board> boards) {
        System.out.println();
        System.out.println("Starting draws");

        for (int ball : drawn) {
            boards.forEach(b -> b.mark(ball));
            Optional<Board> winner = boards.stream().filter(Board::wins).findFirst();
            if (winner.isPresent()) {
                boards.forEach(Board::print);
                System.out.println("Winner: " + winner.get().sum() * ball);
                break;
            }
        }
    }

    private static void part2(int[] drawn, List<Board> boards) {
        for (int ball : drawn) {
            boards.forEach(b -> b.mark(ball));
            if (boards.size() > 1) {
                boards.removeIf(Board::wins);
            } else if (boards.size() == 1 && boards.get(0).wins()) {
                boards.get(0).print();
                System.out.println("Winner: " + boards.get(0).sum() * ball);
                break;
            }
        }
    }

    private static List<Board> getBoards(Scanner scanner) {
        List<Board> boards = new ArrayList<>();
        while (scanner.hasNext()) {
            int[] numbers = new int[Board.SIZE * Board.SIZE];
            scanner.nextLine(); // empty line
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = scanner.nextInt();
            }
            boards.add(new Board(numbers));
        }
        return boards;
    }

}
