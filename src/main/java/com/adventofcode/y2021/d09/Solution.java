package com.adventofcode.y2021.d09;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            int[][] ints = Util.toArray(input);
            part1(ints);
            part2(ints);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int[][] ints) {
        int sum = 0;
        for (int y = 0; y < ints.length; y++) {
            for (int x = 0; x < ints[y].length; x++) {
                if (isLowPoint(ints, x, y)) {
                    sum += ints[y][x] + 1;
                }
            }
        }
        System.out.println(sum);
    }

    private static void part2(int[][] ints) {
        int[][] array = new int[ints.length][ints[0].length];

        //        printArray(ints);

        for (int y = 0; y < ints.length; y++) {
            for (int x = 0; x < ints[y].length; x++) {
                array[y][x] = isFlowPoint(ints, x, y) ? 1 : 0;
            }
        }
        //        printArray(array);

        int cur = 2;
        while (floodFill(array, cur)) {
            cur++;
        }

        //        printArray(array);

        Map<Integer, Long> collect = Arrays.stream(array)
                .flatMapToInt(Arrays::stream)
                .filter(o -> o > 0)
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(collect.values()
                .stream()
                .sorted((o1, o2) -> Long.compare(o2, o1))
                .limit(3)
                .reduce(1L, (o1, o2) -> o1 * o2));
    }

    private static void printArray(int[][] array) {
        System.out.println("Array:");
        Arrays.stream(array).forEach(line -> {
            Arrays.stream(line).forEach(i -> {
                if (i == 0) {
                    System.out.print("--- ");
                } else {
                    System.out.format("%3d ", i);
                }
            });
            System.out.println();
        });
    }

    private static boolean floodFill(int[][] grid, int value) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        for (int y = 0; y < grid.length; y++) {
            for (int x = 0; x < grid[y].length; x++) {
                if (grid[y][x] == 1) {
                    floodFill(grid, visited, value, x, y);
                    return true;
                }
            }
        }
        return false;
    }

    private static void floodFill(int[][] grid, boolean[][] visited, int value, int x, int y) {
        if (y < 0 || y >= grid.length || x < 0 || x >= grid[0].length) {
            return;
        }

        //quit if visited:
        if (visited[y][x]) {
            return;
        }
        visited[y][x] = true;

        if (grid[y][x] == 0) {
            return;
        }

        if (grid[y][x] == 1) {
            grid[y][x] = value;
        }

        //recursively fill in all directions
        floodFill(grid, visited, value, x, y - 1);
        floodFill(grid, visited, value, x + 1, y);
        floodFill(grid, visited, value, x - 1, y);
        floodFill(grid, visited, value, x, y + 1);
    }

    private static boolean isLowPoint(int[][] ints, int x, int y) {
        int cur = ints[y][x];
        int top = Util.getTop(ints, x, y, Integer.MAX_VALUE);
        int left = Util.getLeft(ints, x, y, Integer.MAX_VALUE);
        int bottom = Util.getBottom(ints, x, y, Integer.MAX_VALUE);
        int right = Util.getRight(ints, x, y, Integer.MAX_VALUE);
        return cur < top && cur < left && cur < bottom && cur < right;
    }

    private static boolean isFlowPoint(int[][] ints, int x, int y) {
        int cur = ints[y][x];
        if (cur == 9) {
            return false;
        }
        int top = Util.getTop(ints, x, y, cur);
        int left = Util.getLeft(ints, x, y, cur);
        int bottom = Util.getBottom(ints, x, y, cur);
        int right = Util.getRight(ints, x, y, cur);
        return cur != top || cur != left || cur != bottom || cur != right;
    }

}
