package com.adventofcode.y2021.d18;

public class SnailFishPair implements SnailFish {
    private SnailFish left;
    private SnailFish right;

    public SnailFishPair(SnailFish left, SnailFish right) {
        this.left = left;
        this.right = right;
    }

    public SnailFish getLeft() {
        return left;
    }

    public void setLeft(SnailFish left) {
        this.left = left;
    }

    public SnailFish getRight() {
        return right;
    }

    public void setRight(SnailFish right) {
        this.right = right;
    }

    @Override
    public int getMagnitude() {
        return 3 * left.getMagnitude() + 2 * right.getMagnitude();
    }

    @Override
    public String toString() {
        return "[" + left + "," + right + "]";
    }
}
