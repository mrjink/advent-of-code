package com.adventofcode.y2021.d18;

public class SnailFishNumber implements SnailFish {
    private int value;

    public SnailFishNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getMagnitude() {
        return value;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
