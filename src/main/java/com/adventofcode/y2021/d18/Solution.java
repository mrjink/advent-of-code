package com.adventofcode.y2021.d18;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "test_input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
//        SnailFish snailFish = SnailFish.fromString("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
        SnailFish snailFish = SnailFish.fromString("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]");
        do {
            System.out.println(snailFish);
        } while (reduce(snailFish));
    }

    private static void part2(List<String> input) {
    }

    private static boolean reduce(SnailFish snailFish) {
        return explode(snailFish) || split(snailFish);
    }

    private static boolean explode(SnailFish snailFish) {
        if (!(snailFish instanceof SnailFishPair pair1)) {
            return false;
        }

        System.out.println(pair1);
        return false;
    }

    private static boolean split(SnailFish snailFish) {
        return false;
    }


}
