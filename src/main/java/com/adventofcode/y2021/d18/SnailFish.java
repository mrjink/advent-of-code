package com.adventofcode.y2021.d18;

public interface SnailFish {

    int getMagnitude();

    default SnailFish add(SnailFish other) {
        return new SnailFishPair(this, other);
    }

    static SnailFish fromString(String string) {
        return fromStringBuilder(new StringBuilder(string));
    }

    private static SnailFish fromStringBuilder(StringBuilder stringBuilder) {
        if (stringBuilder.charAt(0) == '[') {
            stringBuilder.deleteCharAt(0);
            SnailFish left = fromStringBuilder(stringBuilder);
            if (stringBuilder.charAt(0) != ',') {
                throw new IllegalArgumentException("Expected ','.");
            }
            stringBuilder.deleteCharAt(0);
            SnailFish right = fromStringBuilder(stringBuilder);
            if (stringBuilder.charAt(0) != ']') {
                throw new IllegalArgumentException("Expected ']'.");
            }
            stringBuilder.deleteCharAt(0);
            return new SnailFishPair(left, right);
        } else {
            int delim = getDelim(stringBuilder);
            int value = Integer.parseInt(stringBuilder.substring(0, delim));
            stringBuilder.delete(0, delim);
            return new SnailFishNumber(value);
        }
    }

    private static int getDelim(StringBuilder stringBuilder) {
        int comma = stringBuilder.indexOf(",");
        int brace = stringBuilder.indexOf("]");
        if (comma == -1 && brace == -1) {
            throw new IllegalArgumentException("Expected ',' or ']'.");
        }
        if (comma == -1) {
            return brace;
        }
        if (brace == -1) {
            return comma;
        }
        return Math.min(comma, brace);
    }
}
