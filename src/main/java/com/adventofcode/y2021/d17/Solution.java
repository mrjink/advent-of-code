package com.adventofcode.y2021.d17;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private static final Pattern PATTERN = Pattern.compile("^target area: x=(-?[0-9]+)\\.\\.(-?[0-9]+), y=(-?[0-9]+)\\.\\.(-?[0-9]+)$");

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            part1("target area: x=20..30, y=-10..-5");
            part1(input);
            part2("target area: x=20..30, y=-10..-5");
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        Target target = getTarget(input);
        int high = 0;
        int xx = 0;
        int yy = 0;
        for (int x = 1; x <= 50; x++) {
            for (int y = 0; y <= 200; y++) {
                int attempt = attempt(target, x, y);
                if (attempt > high) {
                    high = attempt;
                    xx = x;
                    yy = y;
                }
            }
        }
        System.out.format("%d at %d,%d%n", high, xx, yy);
    }

    private static void part2(String input) {
        Target target = getTarget(input);
        int total = 0;
        for (int x = 1; x <= target.maxX; x++) {
            for (int y = -2000; y <= 2000; y++) {
                int attempt = attempt(target, x, y);
                if (attempt >= 0) {
                    total++;
                }
            }
        }
        System.out.println(total);
    }

    private static Target getTarget(String input) {
        Matcher matcher = PATTERN.matcher(input);
        if (!matcher.find()) {
            throw new IllegalArgumentException();
        }
        final int minX = Integer.parseInt(matcher.group(1));
        final int maxX = Integer.parseInt(matcher.group(2));
        final int minY = Integer.parseInt(matcher.group(3));
        final int maxY = Integer.parseInt(matcher.group(4));

        return new Target(minX, minY, maxX, maxY);
    }

    private static int attempt(Target target, int x, int y) {
        int probeX = 0;
        int probeY = 0;
        int velX = x;
        int velY = y;

        int maxY = 0;

        do {
            probeX += velX;
            probeY += velY;
            maxY = Math.max(maxY, probeY);
            velX = Math.max(0, velX - 1);
            velY--;
            // System.out.format("probe: %d,%d; velo: %d,%d%n", probeX, probeY, velX, velY);
            if (probeY < target.minY) {
                return -1;
            }
        } while (!target.contains(probeX, probeY));
        return maxY;
    }

    private record Target(int minX, int minY, int maxX, int maxY) {
        boolean contains(int x, int y) {
            return x >= minX && y >= minY && x <= maxX && y <= maxY;
        }
    }
}
