package com.adventofcode.y2021.d15;

import com.adventofcode.util.Graph;
import com.adventofcode.util.Node;
import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(Util.toArray(input));
            part2(Util.toArray(input));
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int[][] grid) {
        int[][] newGrid = getNewGrid(grid, 1);
        Node[][] nodeGrid = toNodeGrid(newGrid);
        // printGrid(newGrid, "%3d ");
        solve(nodeGrid);
        // printGrid(nodeGrid, "%3d ");
    }

    private static void part2(int[][] grid) {
        int[][] newGrid = getNewGrid(grid, 5);
        Node[][] nodeGrid = toNodeGrid(newGrid);
        // printGrid(newGrid, "%4d ");
        solve(nodeGrid);
        // printGrid(nodeGrid, "%4d ");
    }

    private static int[][] getNewGrid(int[][] grid, int times) {
        int height = grid.length;
        int width = grid[0].length;
        int[][] newGrid = new int[height * times][width * times];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int orig = grid[y][x];
                for (int j = 0; j < times; j++) {
                    for (int i = 0; i < times; i++) {
                        newGrid[j * height + y][i * width + x] = (((orig - 1) + i + j) % 9) + 1;
                    }
                }
            }
        }
        return newGrid;
    }

    private static Node[][] toNodeGrid(int[][] grid) {
        Node[][] nodeGrid = new Node[grid.length][grid[0].length];
        for (int y = 0; y < grid.length; y++) {
            for (int x = 0; x < grid[y].length; x++) {
                nodeGrid[y][x] = new Node(x + "," + y);
            }
        }
        for (int y = 0; y < nodeGrid.length; y++) {
            final int finY = y;
            for (int x = 0; x < nodeGrid[finY].length; x++) {
                final int finX = x;
                Util.getTop(nodeGrid, finX, finY).ifPresent(node -> nodeGrid[finY][finX].addNeighbor(node, grid[finY - 1][finX]));
                Util.getLeft(nodeGrid, finX, finY).ifPresent(node -> nodeGrid[finY][finX].addNeighbor(node, grid[finY][finX - 1]));
                Util.getBottom(nodeGrid, finX, finY).ifPresent(node -> nodeGrid[finY][finX].addNeighbor(node, grid[finY + 1][finX]));
                Util.getRight(nodeGrid, finX, finY).ifPresent(node -> nodeGrid[finY][finX].addNeighbor(node, grid[finY][finX + 1]));
            }
        }
        return nodeGrid;
    }

    private static void solve(Node[][] grid) {
        Node source = grid[0][0];
        Graph.calculateShortestPaths(source);
        System.out.println(grid[grid.length - 1][grid[grid.length - 1].length - 1].getDistance());
    }

    private static void printGrid(int[][] array, String format) {
        System.out.println("Array:");
        Arrays.stream(array).forEach(line -> {
            Arrays.stream(line).forEach(i -> System.out.format(format, i));
            System.out.println();
        });
    }

    private static void printGrid(Node[][] grid, String format) {
        System.out.println("Grid:");
        Arrays.stream(grid).forEach(line -> {
            Arrays.stream(line).forEach(i -> System.out.format(format, i.getDistance()));
            System.out.println();
        });
    }
}
