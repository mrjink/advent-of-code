package com.adventofcode.y2021.d13;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(com.adventofcode.Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Point> points = getPoints(input);
            List<Fold> folds = getFolds(input);
            //            points.forEach(System.out::println);
            //            folds.forEach(System.out::println);
            part1(points, folds);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<Point> points, List<Fold> folds) {
        int maxX = points.stream().mapToInt(p -> p.x).max().orElseThrow();
        int maxY = points.stream().mapToInt(p -> p.y).max().orElseThrow();

        boolean[][] grid = new boolean[maxY + 1][maxX + 1];
        for (Point point : points) {
            grid[point.y][point.x] = true;
        }
        print(grid);
        for (Fold fold : folds) {
            grid = fold(grid, fold);
            print(grid);
            int count = count(grid);
            System.out.println(count);
        }
    }

    private static int count(boolean[][] grid) {
        int count = 0;
        for (boolean[] row : grid) {
            for (boolean b : row) {
                count += b ? 1 : 0;
            }
        }
        return count;
    }

    private static boolean[][] fold(boolean[][] grid, Fold fold) {
        boolean[][] folded;
        if (fold instanceof Vertical) {
            folded = new boolean[grid.length][(grid[0].length / 2)];
            for (int y = 0; y < folded.length; y++) {
                for (int x = 0; x < folded[y].length; x++) {
                    folded[y][x] = grid[y][x] || grid[y][grid[y].length - x - 1];
                }
            }
        } else if (fold instanceof Horizontal) {
            folded = new boolean[(grid.length / 2)][grid[0].length];
            for (int y = 0; y < folded.length; y++) {
                for (int x = 0; x < folded[y].length; x++) {
                    folded[y][x] = grid[y][x] || grid[grid.length - y - 1][x];
                }
            }
        } else {
            throw new IllegalArgumentException(String.valueOf(fold));
        }
        return folded;
    }

    private static void print(boolean[][] grid) {
        System.out.println();
        System.out.println("Start of grid");
        for (boolean[] line : grid) {
            for (boolean dot : line) {
                System.out.print(dot ? "#" : ".");
            }
            System.out.println();
        }
        System.out.println("End of grid");
    }

    private static List<Point> getPoints(List<String> input) {
        List<Point> points = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split(",");
            if (split.length != 2) {
                continue;
            }
            points.add(new Point(Integer.parseInt(split[0]), Integer.parseInt(split[1])));
        }
        return points;
    }

    private static List<Fold> getFolds(List<String> input) {
        List<Fold> folds = new ArrayList<>();
        Pattern foldPattern = Pattern.compile("^fold along ([xy])=([0-9]+)$");
        for (String line : input) {
            Matcher matcher = foldPattern.matcher(line);
            if (matcher.find()) {
                switch (matcher.group(1)) {
                    case "x" -> folds.add(new Vertical(Integer.parseInt(matcher.group(2))));
                    case "y" -> folds.add(new Horizontal(Integer.parseInt(matcher.group(2))));
                }
            }
        }
        return folds;
    }

    private static void part2(List<String> input) {
    }

    private record Point(int x, int y) {
    }

    private interface Fold {
    }

    private record Horizontal(int y) implements Fold {
    }

    private record Vertical(int x) implements Fold {
    }
}
