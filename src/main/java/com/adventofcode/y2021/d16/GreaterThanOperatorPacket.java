package com.adventofcode.y2021.d16;

public class GreaterThanOperatorPacket extends OperatorPacket {
    public GreaterThanOperatorPacket(int packetVersion) {
        super(packetVersion, PacketType.GREATER_THAN);
    }

    @Override
    public long getValue() {
        long v1 = getSubPackets().get(0).getValue();
        long v2 = getSubPackets().get(1).getValue();
        return v1 > v2 ? 1 : 0;
    }
}
