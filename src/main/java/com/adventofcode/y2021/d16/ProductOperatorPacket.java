package com.adventofcode.y2021.d16;

public class ProductOperatorPacket extends OperatorPacket {
    public ProductOperatorPacket(int packetVersion) {
        super(packetVersion, PacketType.PRODUCT);
    }

    @Override
    public long getValue() {
        return getSubPackets().stream().mapToLong(Packet::getValue).reduce(1, (left, right) -> left * right);
    }
}
