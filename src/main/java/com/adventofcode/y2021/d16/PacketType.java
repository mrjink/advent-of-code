package com.adventofcode.y2021.d16;

public enum PacketType {
    SUM("sum") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new SumOperatorPacket(packetVersion);
        }
    },
    PRODUCT("prod") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new ProductOperatorPacket(packetVersion);
        }
    },
    MINIMUM("min") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new MinOperatorPacket(packetVersion);
        }
    },
    MAXIMUM("max") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new MaxOperatorPacket(packetVersion);
        }
    },
    LITERAL("val") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return null;
        }
    },
    GREATER_THAN("gt") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new GreaterThanOperatorPacket(packetVersion);
        }
    },
    LESS_THAN("lt") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new LessThanOperatorPacket(packetVersion);
        }
    },
    EQUAL_TO("eq") {
        @Override
        public OperatorPacket getOperatorPacket(int packetVersion) {
            return new EqualToOperatorPacket(packetVersion);
        }
    };

    private final String string;

    PacketType(String string) {
        this.string = string;
    }

    public abstract OperatorPacket getOperatorPacket(int packetVersion);

    @Override
    public String toString() {
        return string;
    }
}
