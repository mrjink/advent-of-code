package com.adventofcode.y2021.d16;

public class MaxOperatorPacket extends OperatorPacket {
    public MaxOperatorPacket(int packetVersion) {
        super(packetVersion, PacketType.MAXIMUM);
    }

    @Override
    public long getValue() {
        return getSubPackets().stream().mapToLong(Packet::getValue).max().orElseThrow();
    }
}
