package com.adventofcode.y2021.d16;

public class MinOperatorPacket extends OperatorPacket {
    public MinOperatorPacket(int packetVersion) {
        super(packetVersion, PacketType.MINIMUM);
    }

    @Override
    public long getValue() {
        return getSubPackets().stream().mapToLong(Packet::getValue).min().orElseThrow();
    }
}
