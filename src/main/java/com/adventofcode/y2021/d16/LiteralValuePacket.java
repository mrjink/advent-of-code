package com.adventofcode.y2021.d16;

public class LiteralValuePacket extends AbstractPacket {
    private final long value;

    protected LiteralValuePacket(int packetVersion, long value) {
        super(packetVersion, PacketType.LITERAL);
        this.value = value;
    }

    @Override
    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{" + super.toString() + ", lv=" + value + "}";
    }
}
