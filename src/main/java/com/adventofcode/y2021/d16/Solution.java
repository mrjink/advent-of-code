package com.adventofcode.y2021.d16;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            part1("D2FE28");
            part1("38006F45291200");
            part1("EE00D40C823060");
            part1("8A004A801A8002F478");
            part1("620080001611562C8802118E34");
            part1("C0015000016115A2E0802F182340");
            part1("A0016C880162017C3686B18A3D4780");
            part1(input);

            part2("C200B40A82");
            part2("04005AC33890");
            part2("880086C3E88112");
            part2("CE00C43D881120");
            part2("D8005AC2A8F0");
            part2("F600BC2D8F");
            part2("9C005AC2F8F0");
            part2("9C0141080250320F1802104A08");
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(String input) {
        System.out.println("Input: " + input);
        Transmission transmission = Transmission.fromString(input);
        Packet packet = transmission.getPacket();
        System.out.println(packet);
        System.out.println("Versions: " + sumVersions(packet));
        System.out.println();
    }

    private static void part2(String input) {
        System.out.println("Input: " + input);
        Transmission transmission = Transmission.fromString(input);
        Packet packet = transmission.getPacket();
        System.out.println(packet);
        System.out.println("Value: " + packet.getValue());
        System.out.println();
    }

    private static int sumVersions(Packet packet) {
        int result = packet.getPacketVersion();
        if (packet instanceof OperatorPacket operatorPacket) {
            result += operatorPacket.getSubPackets().stream().mapToInt(Solution::sumVersions).sum();
        }
        return result;
    }
}
