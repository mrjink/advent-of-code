package com.adventofcode.y2021.d16;

public abstract class AbstractPacket implements Packet {
    private final int packetVersion;
    private final PacketType packetType;

    protected AbstractPacket(int packetVersion, PacketType packetType) {
        this.packetVersion = packetVersion;
        this.packetType = packetType;
    }

    @Override
    public final int getPacketVersion() {
        return packetVersion;
    }

    @Override
    public final PacketType getPacketType() {
        return packetType;
    }

    @Override
    public abstract long getValue();

    @Override
    public String toString() {
        return "pv=" + packetVersion + ", pt=" + packetType;
    }
}
