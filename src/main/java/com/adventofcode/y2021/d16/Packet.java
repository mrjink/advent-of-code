package com.adventofcode.y2021.d16;

public interface Packet {

    int getPacketVersion();

    PacketType getPacketType();

    long getValue();
}
