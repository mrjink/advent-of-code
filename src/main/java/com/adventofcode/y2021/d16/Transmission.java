package com.adventofcode.y2021.d16;

public class Transmission {

    private String transmission;
    private int position;

    private Transmission(String transmission) {
        this.transmission = transmission;
        this.position = 0;
    }

    public static Transmission fromString(String hexString) {
        return new Transmission(toBits(hexString));
    }

    public Packet getPacket() {
        int packetVersion = getPacketVersion();
        PacketType packetType = getPacketType();
        if (packetType == PacketType.LITERAL) {
            return new LiteralValuePacket(packetVersion, getLiteralValue());
        }
        OperatorPacket operatorPacket = packetType.getOperatorPacket(packetVersion);
        int lengthTypeId = getPacket(1);
        if (lengthTypeId == 0) {
            int subPacketLength = getPacket(15);
            int startPosition = position;
            while (position < startPosition + subPacketLength && position < transmission.length()) {
                operatorPacket.addPacket(getPacket());
            }
        } else {
            int numPackets = getPacket(11);
            for (int i = 0; i < numPackets; i++) {
                operatorPacket.addPacket(getPacket());
            }
        }
        return operatorPacket;
    }

    private int getPacketVersion() {
        return getPacket(3);
    }

    private PacketType getPacketType() {
        int packetType = getPacket(3);
        return PacketType.values()[packetType];
    }

    private long getLiteralValue() {
        long value = 0;
        int packet;
        do {
            packet = getPacket(5);
            value = (value << 4) | (packet & 0b1111);
        } while ((packet & 0b10000) == 0b10000);
        return value;
    }

    private int getPacket(int size) {
        int packet = toInt(transmission.substring(position, position + size));
        position += size;
        return packet;
    }

    @Override
    public String toString() {
        return "Transmission{transmission='" + transmission + '\'' + '}';
    }

    private static int toInt(String binaryString) {
        return Integer.parseInt(binaryString, 2);
    }

    static String toBits(String string) {
        StringBuilder result = new StringBuilder();
        for (char c : string.toCharArray()) {
            result.append(getValue(c));
        }
        return result.toString();
    }

    private static String getValue(char c) {
        return switch (c) {
            case '0' -> "0000";
            case '1' -> "0001";
            case '2' -> "0010";
            case '3' -> "0011";
            case '4' -> "0100";
            case '5' -> "0101";
            case '6' -> "0110";
            case '7' -> "0111";
            case '8' -> "1000";
            case '9' -> "1001";
            case 'A' -> "1010";
            case 'B' -> "1011";
            case 'C' -> "1100";
            case 'D' -> "1101";
            case 'E' -> "1110";
            case 'F' -> "1111";
            default -> throw new IllegalStateException("Unexpected value: " + c);
        };
    }
}
