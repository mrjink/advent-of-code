package com.adventofcode.y2021.d16;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class OperatorPacket extends AbstractPacket {
    private final List<Packet> subPackets;

    protected OperatorPacket(int packetVersion, PacketType packetType) {
        super(packetVersion, packetType);
        subPackets = new ArrayList<>();
    }

    public void addPacket(Packet packet) {
        this.subPackets.add(packet);
    }

    public List<Packet> getSubPackets() {
        return Collections.unmodifiableList(subPackets);
    }

    @Override
    public String toString() {
        return "{" + super.toString() + ", sp=" + subPackets.toString() + "}";
    }
}
