package com.adventofcode.y2021.d16;

public class SumOperatorPacket extends OperatorPacket {
    protected SumOperatorPacket(int packetVersion) {
        super(packetVersion, PacketType.SUM);
    }

    @Override
    public long getValue() {
        return getSubPackets().stream().mapToLong(Packet::getValue).sum();
    }
}
