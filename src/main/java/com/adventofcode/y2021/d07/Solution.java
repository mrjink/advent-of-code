package com.adventofcode.y2021.d07;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.ToLongBiFunction;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            String input = Util.readFile(Solution.class, "input");
            Map<Integer, Long> crabs = getCrabs(input);
            part1(crabs);
            part2(crabs);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static Map<Integer, Long> getCrabs(String input) {
        return Arrays.stream(input.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.groupingBy(Function.identity(), TreeMap::new, Collectors.counting()));
    }

    private static void part1(Map<Integer, Long> crabs) {
        solve(crabs, (e, i) -> {
            long diff = Math.abs(e.getKey() - i);
            return diff * e.getValue();
        });
    }

    private static void part2(Map<Integer, Long> crabs) {
        solve(crabs, (e, i) -> {
            long diff = Math.abs(e.getKey() - i);
            return ((diff * (diff + 1)) >> 1) * e.getValue();
        });
    }

    private static void solve(Map<Integer, Long> crabs, ToLongBiFunction<Map.Entry<Integer, Long>, Long> getFuel) {
        int min = crabs.keySet().stream().min(Integer::compareTo).orElse(-1);
        int max = crabs.keySet().stream().max(Integer::compareTo).orElse(-1);
        long sum = Long.MAX_VALUE;
        for (int i = min; i <= max; i++) {
            final long finalI = i;
            sum = Math.min(sum,
                    crabs.entrySet()
                            .stream()
                            .mapToLong(e -> getFuel.applyAsLong(e, finalI))
                            .sum()
            );
        }
        System.out.println(sum);
    }
}
