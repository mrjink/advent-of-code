package com.adventofcode.y2021.d05;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);
    private static final Pattern PATTERN = Pattern.compile("^([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)$");

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            List<Line> lines = getLines(input);
            part1(lines);
            part2(lines);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static List<Line> getLines(List<String> input) {
        List<Line> lines = new ArrayList<>();
        for (String line : input) {
            Matcher matcher = PATTERN.matcher(line);
            if (matcher.find()) {
                lines.add(new Line(
                        new Point(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2))),
                        new Point(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)))));
            } else {
                System.out.println("Can't parse " + line);
            }
        }
        return lines;
    }

    private static void part1(List<Line> lines) {
        int[][] grid = getGrid(lines);

        for (Line line : lines) {
            if (line.p1.x == line.p2.x) {
                vertical(grid, line);
            } else if (line.p1.y == line.p2.y) {
                horizontal(grid, line);
            }
        }
        printGrid(grid);
        System.out.println(overlaps(grid));
    }

    private static void part2(List<Line> lines) {
        int[][] grid = getGrid(lines);

        for (Line line : lines) {
            if (line.p1.x == line.p2.x) {
                vertical(grid, line);
            } else if (line.p1.y == line.p2.y) {
                horizontal(grid, line);
            } else {
                diagonal(grid, line);
            }
        }
        printGrid(grid);
        System.out.println(overlaps(grid));
    }

    private static int[][] getGrid(List<Line> lines) {
        int maxX = lines.stream().mapToInt(l -> Math.max(l.p1.x, l.p2.x)).max().orElse(-1);
        int maxY = lines.stream().mapToInt(l -> Math.max(l.p1.y, l.p2.y)).max().orElse(-1);
        return new int[maxY + 1][maxX + 1];
    }

    private static void vertical(int[][] grid, Line line) {
        System.out.println("V: " + line);
        for (int y = Math.min(line.p1.y, line.p2.y); y <= Math.max(line.p1.y, line.p2.y); y++) {
            grid[y][line.p1.x]++;
        }
    }

    private static void horizontal(int[][] grid, Line line) {
        System.out.println("H: " + line);
        for (int x = Math.min(line.p1.x, line.p2.x); x <= Math.max(line.p1.x, line.p2.x); x++) {
            grid[line.p1.y][x]++;
        }
    }

    private static void diagonal(int[][] grid, Line line) {
        System.out.println("D: " + line);
        int y = line.p1.y;
        int dy = line.p1.y < line.p2.y ? 1 : -1;
        if (line.p1.x < line.p2.x) {
            for (int x = line.p1.x; x <= line.p2.x; x++, y += dy) {
                grid[y][x]++;
            }
        } else {
            for (int x = line.p1.x; x >= line.p2.x; x--, y += dy) {
                grid[y][x]++;
            }
        }
    }

    private static void printGrid(int[][] grid) {
        for (int[] row : grid) {
            for (int cell : row) {
                System.out.print(cell == 0 ? "." : "" + cell);
            }
            System.out.println();
        }
        System.out.println();
    }

    private static long overlaps(int[][] grid) {
        return Arrays.stream(grid)
                .mapToLong(row -> IntStream.of(row)
                        .filter(i -> i > 1)
                        .count())
                .sum();
    }

    private record Point(int x, int y) {
    }

    private record Line(Point p1, Point p2) {
    }
}
