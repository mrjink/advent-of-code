package com.adventofcode.y2021.d01;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            int[] input = Util.readAllInts(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(int[] input) throws URISyntaxException, IOException {
        int count = 0;

        int prev = input[0];
        for (int i = 1; i < input.length; i++) {
            if (input[i] > prev) {
                count++;
            }
            prev = input[i];
        }
        System.out.println(count);
    }

    private static void part2(int[] input) throws URISyntaxException, IOException {
        int count = 0;

        int prev = input[0] + input[1] + input[2];
        for (int i = 1; i < input.length - 2; i++) {
            int next = input[i] + input[i + 1] + input[i + 2];
            if (next > prev) {
                count++;
            }
            prev = next;
        }
        System.out.println(count);
    }
}
