package com.adventofcode.y2021.d03;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        int s = input.get(0).length();
        int[] zero = new int[s];
        int[] ones = new int[s];
        countBits(input, zero, ones);

        int epsilon = 0;
        int gamma = 0;
        for (int i = 0; i < s; i++) {
            epsilon <<= 1;
            gamma <<= 1;
            if (ones[i] >= zero[i]) {
                gamma++;
            } else {
                epsilon++;
            }
        }
        System.out.println(gamma);
        System.out.println(epsilon);
        System.out.println(gamma * epsilon);
    }

    private static void part2(List<String> input) {
        int s = input.get(0).length();
        int[] zero = new int[s];
        int[] ones = new int[s];
        List<String> copy = new ArrayList<>(input);
        String prefix = "";
        while (copy.size() > 1) {
            countBits(copy, zero, ones);
            if (ones[prefix.length()] >= zero[prefix.length()]) {
                prefix += "1";
            } else {
                prefix += "0";
            }
            copy = copy(copy, prefix);
        }
        int oxygen = getValue(copy.get(0));
        System.out.format("%s %d%n", copy.get(0), oxygen);

        copy = new ArrayList<>(input);
        prefix = "";
        while (copy.size() > 1) {
            countBits(copy, zero, ones);
            if (ones[prefix.length()] >= zero[prefix.length()]) {
                prefix += "0";
            } else {
                prefix += "1";
            }
            copy = copy(copy, prefix);
        }
        int co2 = getValue(copy.get(0));
        System.out.format("%s %d%n", copy.get(0), co2);

        System.out.println(oxygen * co2);
    }

    private static int getValue(String string) {
        int result = 0;
        for (int i = 0; i < string.length(); i++) {
            result <<= 1;
            if (string.charAt(i) == '1') {
                result++;
            }
        }
        return result;
    }

    private static List<String> copy(List<String> copy, String prefix) {
        List<String> result = new ArrayList<>();
        for (String s : copy) {
            if (s.startsWith(prefix)) {
                result.add(s);
            }
        }
        return result;
    }

    private static void countBits(List<String> input, int[] zero, int[] ones) {
        Arrays.fill(zero, 0);
        Arrays.fill(ones, 0);
        for (String line : input) {
            for (int i = 0; i < zero.length; i++) {
                switch (line.charAt(i)) {
                    case '0' -> zero[i]++;
                    case '1' -> ones[i]++;
                }
            }
        }
    }
}
