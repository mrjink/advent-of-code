package com.adventofcode.y2021.d10;

import com.adventofcode.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Solution {
    private static final Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    private enum BRACE {
        R, S, C, A;
    }

    public static void main(String[] args) {
        try {
            List<String> input = Util.readAllLines(Solution.class, "input");
            part1(input);
            part2(input);
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Nope", e);
        }
    }

    private static void part1(List<String> input) {
        long score = 0L;
        for (String line : input) {
            score += getScore1(line);
        }
        System.out.println(score);
    }

    private static long getScore1(String line) {
        Stack<BRACE> braces = new Stack<>();

        for (int i = 0; i < line.length(); i++) {
            char x = line.charAt(i);
            switch (x) {
                case '(' -> braces.push(BRACE.R);
                case '[' -> braces.push(BRACE.S);
                case '{' -> braces.push(BRACE.C);
                case '<' -> braces.push(BRACE.A);

                case ')' -> {
                    if (braces.pop() != BRACE.R) {
                        return 3L;
                    }
                }
                case ']' -> {
                    if (braces.pop() != BRACE.S) {
                        return 57L;
                    }
                }
                case '}' -> {
                    if (braces.pop() != BRACE.C) {
                        return 1197L;
                    }
                }
                case '>' -> {
                    if (braces.pop() != BRACE.A) {
                        return 25137L;
                    }
                }
            }
        }

        return 0L;
    }

    private static long getScore2(String line) {
        Stack<BRACE> braces = new Stack<>();

        for (int i = 0; i < line.length(); i++) {
            char x = line.charAt(i);
            switch (x) {
                case '(' -> braces.push(BRACE.R);
                case '[' -> braces.push(BRACE.S);
                case '{' -> braces.push(BRACE.C);
                case '<' -> braces.push(BRACE.A);

                case ')' -> {
                    if (braces.pop() != BRACE.R) {
                        return 0L;
                    }
                }
                case ']' -> {
                    if (braces.pop() != BRACE.S) {
                        return 0L;
                    }
                }
                case '}' -> {
                    if (braces.pop() != BRACE.C) {
                        return 0L;
                    }
                }
                case '>' -> {
                    if (braces.pop() != BRACE.A) {
                        return 0L;
                    }
                }
            }
        }

        if (braces.isEmpty()) {
            return 0L;
        }
        long score = 0;
        do {
            score = score * 5 + (braces.pop().ordinal() + 1);
        } while (!braces.isEmpty());
        return score;
    }

    private static void part2(List<String> input) {
        List<Long> scores = new ArrayList<>();
        for (String line : input) {
            scores.add(getScore2(line));
        }
        List<Long> sorted = scores.stream().filter(l -> l > 0).sorted().toList();
        System.out.println(sorted.get(sorted.size() / 2));
    }
}
